import React from "react";
import { navigationKeywords } from "../../assets/navigationKeywords";
import { Card } from "semantic-ui-react";
import { templateData } from "./fakeTemplateData";
import "../../styles/menuTemplate.css";

export default class MenuTemplatePage extends React.Component {
  constructor(props) {
    super();
    this.state = {};
  }

  selectTemplate = templateInfo => {
    this.props.templateItemClicked(templateInfo);
  };

  render() {
    return (
      <div
        className={
          "containerPage menuTemplatePage " +
          (this.props.navigation.currentPage ===
          navigationKeywords.MENU_TEMPLATE
            ? ""
            : "inActive")
        }
      >
        <Card id="templateContainer">
          <Card.Header>
            <p id="changeFormatButton">Change Menu Format</p>
            <p id="pageTitle">Choose Template</p>
            <p id="formatInfo">Newspaper: 27.9 x 43.2 (11 x 17)</p>
          </Card.Header>
          <Card.Content>
            <div>
              {templateData.map((templateInfo, index) => (
                <TemplateItem
                  selectTemplate={this.selectTemplate}
                  templateInfo={templateInfo}
                  key={index}
                />
              ))}
            </div>
            <div id="bottomLayer" />
          </Card.Content>
        </Card>
      </div>
    );
  }
}

const TemplateItem = ({ templateInfo, selectTemplate }) => {
  return (
    <div onClick={() => selectTemplate(templateInfo)} className="templateItem">
      <img src={templateInfo.imageURL} alt={templateInfo.templateName} />
      <p>{templateInfo.templateName}</p>
    </div>
  );
};
