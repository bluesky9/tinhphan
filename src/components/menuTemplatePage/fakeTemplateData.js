export const templateData = [
  {
    templateName: "Blank",
    imageURL: "/templateImages/blank.png"
  },
  {
    templateName: "Fresh",
    imageURL: "/templateImages/fresh.png"
  },
  {
    templateName: "Light Green",
    imageURL: "/templateImages/lightGreen.png"
  },
  {
    templateName: "Hot Red",
    imageURL: "/templateImages/hotred.png"
  },
  {
    templateName: "Asian",
    imageURL: "/templateImages/asian.png"
  },
  {
    templateName: "Coffee and Tea",
    imageURL: "/templateImages/coffeeNTea.png"
  },
  {
    templateName: "Kids",
    imageURL: "/templateImages/kids5.png"
  },
  {
    templateName: "Vintage",
    imageURL: "/templateImages/vintage.png"
  },
  {
    templateName: "Kids",
    imageURL: "/templateImages/kids4.png"
  },
  {
    templateName: "Kids",
    imageURL: "/templateImages/kids3.png"
  },
  {
    templateName: "Kids",
    imageURL: "/templateImages/kids2.png"
  },
  {
    templateName: "Kids",
    imageURL: "/templateImages/kids1.png"
  },
  {
    templateName: "Fresh",
    imageURL: "/templateImages/fresh.png"
  },
  {
    templateName: "Light Green",
    imageURL: "/templateImages/lightGreen.png"
  },
  {
    templateName: "Hot Red",
    imageURL: "/templateImages/hotred.png"
  },
  {
    templateName: "Asian",
    imageURL: "/templateImages/asian.png"
  },
  {
    templateName: "Coffee and Tea",
    imageURL: "/templateImages/coffeeNTea.png"
  },
  {
    templateName: "Kids",
    imageURL: "/templateImages/kids5.png"
  },
  {
    templateName: "Vintage",
    imageURL: "/templateImages/vintage.png"
  },
  {
    templateName: "Kids",
    imageURL: "/templateImages/kids4.png"
  },
  {
    templateName: "Kids",
    imageURL: "/templateImages/kids3.png"
  },
  {
    templateName: "Kids",
    imageURL: "/templateImages/kids2.png"
  },
  {
    templateName: "Kids",
    imageURL: "/templateImages/kids1.png"
  }
];
