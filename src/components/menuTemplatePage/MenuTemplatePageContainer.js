import { connect } from "react-redux";
import { templateItemClicked } from "../../redux/actions";
import MenuTemplatePage from "./MenuTemplatePage";
// import Redux Actions with the following line
// import {} from  ''

const mapStateToProps = (state, ownProps) => {
  return Object.assign(
    {},
    {
      navigation: state.navigation
    }
  );
};

const mapDispatchToProps = dispatch => {
  return {
    templateItemClicked: templateInfo =>
      dispatch(templateItemClicked(templateInfo))
  };
};

const MenuTemplatePageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(MenuTemplatePage);

export default MenuTemplatePageContainer;
