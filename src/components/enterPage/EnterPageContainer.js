import { connect } from "react-redux";
import { menuCreationHandler } from "../../redux/actions";
import EnterPage from "./EnterPage";
// import Redux Actions with the following line
// import {} from  ''

const mapStateToProps = (state, ownProps) => {
  return Object.assign(
    {},
    {
      navigation: state.navigation
    }
  );
};

const mapDispatchToProps = dispatch => {
  return {
    createMenu: menuInfo => dispatch(menuCreationHandler(menuInfo))
  };
};

const EnterPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(EnterPage);

export default EnterPageContainer;
