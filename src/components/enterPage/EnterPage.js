import React from "react";
import PropTypes from "prop-types";
import { Button, Form, Divider, Dropdown, Message } from "semantic-ui-react";
import { navigationKeywords } from "../../assets/navigationKeywords";
import "../../styles/enterPage.css";

const MenuItems = [
  { value: "all", text: "All" },
  { value: "articles", text: "Articles" },
  { value: "products", text: "Products" }
];
const landscapeMenuFormats = [
  {
    text: "Newspaper: 43.2 x 27.9 (17 x 11)",
    value: "newspaper"
  },
  {
    text: "Legal: 35.6 x 21.6 (14 x 8.5)",
    value: "legal"
  },
  {
    text: "A4: 29.7 x 21.0 (11.7 x 8.3)",
    value: "a4"
  },
  {
    text: "Letter: 27.9 x 21.6 (11 x 8.5)",
    value: "letter"
  },
  {
    text: "A5: 21.0 x 14.8 (8.3 x 5.8)",
    value: "a5"
  }
];
const portraitMenuFormats = [
  {
    text: "Newspaper: 27.9 x 43.2 (11 x 17)",
    value: "newspaper"
  },
  {
    text: "Legal: 21.6 x 35.6 (8.5 x 14)",
    value: "legal"
  },
  {
    text: "A4: 21.0 x 29.7 (8.3 x 11.7)",
    value: "a4"
  },
  {
    text: "Letter: 21.6 x 27.9 (8.5 x 11)",
    value: "letter"
  },
  {
    text: "A5: 14.8 x 21.0 (5.8 x 8.3)",
    value: "a5"
  }
];
const bookletMenuFormats = [
  {
    text: "A4 landscape: 29.7 x 21.0 (11.7 x 8.3)",
    value: "a4l"
  },
  {
    text: "A4 portrait: 21.0 x 29.7 (8.3 x 11.7)",
    value: "a4p"
  },
  {
    text: "A5 landscape: 21.0 x 14.8 (8.3 x 5.8)",
    value: "a5l"
  },
  {
    text: "A5 portrait: 14.8 x 21.0 (5.8 x 8.3)",
    value: "a5p"
  }
];
const foldedMenuFormats = [
  {
    text: "A4 Bifold: 29.7 x 21.0 (11.7 x 8.3)",
    value: "a4"
  },
  {
    text: "Letter Bifold: 27.9 x 21.6 (11 x 8.5)",
    value: "letter"
  },
  {
    text: "Newspaper Trifold: 43.2 x 27.9 (17 x 11)",
    value: "newspaper"
  },
  {
    text: "Legal Trifold: 35.6 x 21.6 (14 x 8.5)",
    value: "legal"
  }
];
export default class EnterPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      menuName: "",
      menuType: "foodMenu",
      menuItem: "all",
      menuFormat: "landscape",
      menuFormatItem: "newspaper"
    };
  }

  componentWillMount() {
    console.log(this.props);
  }

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
    if (value !== 0) {
      this.setState({ error: false });
    }
  };

  onChange = (e, data) => {
    //console.log(data);
    switch (data.name) {
      case "menuType":
        this.setState({ menuType: data.value });
        break;
      case "menuItem":
        this.setState({ menuItem: data.value });
        break;
      case "menuFormat":
        let menuFormatItem = this.state.menuFormatItem;
        switch (data.value) {
          case "portrait":
            menuFormatItem = "newspaper";
            break;
          case "booklet":
            menuFormatItem = "a4l";
            break;
          case "folded":
            menuFormatItem = "a4";
            break;
          default:
            menuFormatItem = "newspaper";
        }
        this.setState({ menuFormat: data.value, menuFormatItem });
        break;
      default:
        this.setState({ menuFormatItem: data.value });
    }
  };

  handleSubmit = e => {
    const formData = {
      menuName: this.state.menuName,
      menuType: this.state.menuType,
      menuItem: this.state.menuItem,
      menuFormat: this.state.menuFormat,
      menuFormatItem: this.state.menuFormatItem
    };

    if (formData.menuName.length === 0) {
      console.log("error");
      return this.setState({ error: true });
    }

    this.props.createMenu(formData);
  };
  render() {
    const { menuItem, menuFormatItem } = this.state;
    return (
      <div
        className={
          "containerPage body " +
          (this.props.navigation.currentPage === navigationKeywords.ENRTER_PAGE
            ? ""
            : "inActive")
        }
      >
        <div className="enterPage">
          <p id="title">Create Your First Menu</p>
          <Form error={this.state.error} onSubmit={this.handleSubmit}>
            <Message error header="Menu Name is Required" />
            <div className="label">MENU NAME</div>
            <Form.Input
              className="menuName"
              error={this.state.error && this.state.menuName.length === 0}
              name="menuName"
              placeholder="Enter menu name here"
              onChange={this.handleChange}
            />
            <Divider className="divider" />
            <div class="labelType">MENU TYPE</div>
            <Button.Group id="button-group" fluid inverted color="orange">
              <Button
                name="menuType"
                type="button"
                active={this.state.menuType === "foodMenu"}
                value="foodMenu"
                onClick={this.onChange}
              >
                Food Menu
              </Button>
              <Button
                name="menuType"
                type="button"
                active={this.state.menuType === "wineMenu"}
                value="wineMenu"
                onClick={this.onChange}
              >
                Wine Menu
              </Button>
            </Button.Group>
            <Divider className="divider" />
            <div class="labelItemSet">MENU ITEM SET</div>
            <div>
              <Dropdown
                id="dropdown"
                name="menuItem"
                placeholder="Select Menu Item"
                selection
                search
                options={MenuItems}
                value={menuItem}
                onChange={this.onChange}
              />
              <img
                className="iconDropdown"
                src="\basicImages\enterDropdownIcon.png"
                alt="Dropdown Icon"
              />
            </div>
            <Divider className="divider" />
            <div class="labelFormat">MENU FORMAT</div>
            <Button.Group id="button-group" fluid inverted color="orange">
              <Button
                name="menuFormat"
                type="button"
                active={this.state.menuFormat === "landscape"}
                value="landscape"
                onClick={this.onChange}
              >
                Landscape
              </Button>
              <Button
                name="menuFormat"
                type="button"
                active={this.state.menuFormat === "portrait"}
                value="portrait"
                onClick={this.onChange}
              >
                Portrait
              </Button>
              <Button
                name="menuFormat"
                type="button"
                active={this.state.menuFormat === "booklet"}
                value="booklet"
                onClick={this.onChange}
              >
                Booklet
              </Button>
              <Button
                name="menuFormat"
                type="button"
                active={this.state.menuFormat === "folded"}
                value="folded"
                onClick={this.onChange}
              >
                Folded
              </Button>
            </Button.Group>
            <br />
            <div>
              <Dropdown
                id="dropdown"
                name="menuFormatItem"
                placeholder="Select Menu Item"
                selection
                search
                options={
                  this.state.menuFormat === "landscape"
                    ? landscapeMenuFormats
                    : this.state.menuFormat === "portrait"
                      ? portraitMenuFormats
                      : this.state.menuFormat === "booklet"
                        ? bookletMenuFormats
                        : this.state.menuFormat === "folded"
                          ? foldedMenuFormats
                          : null
                }
                value={menuFormatItem}
                onChange={this.onChange}
              />
              <img
                className="iconDropdown"
                src="\basicImages\enterDropdownIcon.png"
                alt="Dropdown Icon"
              />
            </div>
            <Divider className="divider" />
            <Button id="next">Next</Button>
          </Form>
        </div>
      </div>
    );
  }
}
EnterPage.propTypes = {
  onClick: PropTypes.func.isRequired,
  error: PropTypes.string.isRequired,
  isAuthenticated: PropTypes.bool.isRequired
};
