import { connect } from "react-redux";
import { navigateEditorSection } from "../../../redux/actions";
import SectionSidebar from "./SectionSidebar";
// import Redux Actions with the following line
// import {} from  ''

const mapStateToProps = (state, ownProps) => {
  return Object.assign(
    {},
    {
      navigation: state.navigation
    }
  );
};

const mapDispatchToProps = dispatch => {
  return {
    navigateEditorSection: editorSection =>
      dispatch(navigateEditorSection(editorSection))
  };
};

const SectionSidebarContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SectionSidebar);

export default SectionSidebarContainer;
