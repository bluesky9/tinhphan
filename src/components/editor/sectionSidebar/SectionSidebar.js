import React from "react";
import { editorSections } from "../../../assets/navigationKeywords";
import "../../../styles/editor/sectionSidebar.css";

const sectionButtonsData = [
  {
    section: editorSections.LAYOUT,
    imageURL: "/basicImages/sectionLayout.png",
    activeImageURL: "/basicImages/sectionLayoutActive.png"
  },
  {
    section: editorSections.SETUP,
    imageURL: "/basicImages/sectionSetup.png",
    activeImageURL: "/basicImages/sectionSetupActive.png"
  },
  {
    section: editorSections.FORMAT,
    imageURL: "/basicImages/sectionFormat.png",
    activeImageURL: "/basicImages/sectionFormatActive.png"
  },
  {
    section: editorSections.TEXT,
    imageURL: "/basicImages/sectionText.png",
    activeImageURL: "/basicImages/sectionTextActive.png"
  },
  {
    section: editorSections.ELEMENT,
    imageURL: "/basicImages/sectionElement.png",
    activeImageURL: "/basicImages/sectionElementActive.png"
  },
  {
    section: editorSections.BACKGROUND,
    imageURL: "/basicImages/sectionBackground.png",
    activeImageURL: "/basicImages/sectionBackgroundActive.png"
  }
];

const SectionSidebar = props => {
  return (
    <div className="sectionSidebar">
      {sectionButtonsData.map((sectionInfo, index) => (
        <div
          className={
            "sectionButton " +
            (props.navigation.editorSection === sectionInfo.section
              ? "active"
              : "")
          }
          onClick={() => props.navigateEditorSection(sectionInfo.section)}
        >
          <img
            src={sectionInfo.imageURL}
            alt={sectionInfo.section}
            className={
              props.navigation.editorSection !== sectionInfo.section
                ? ""
                : "inActive"
            }
          />
          <img
            src={sectionInfo.activeImageURL}
            alt={sectionInfo.section}
            className={
              props.navigation.editorSection === sectionInfo.section
                ? ""
                : "inActive"
            }
          />
        </div>
      ))}
    </div>
  );
};

export default SectionSidebar;
