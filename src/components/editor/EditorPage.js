import React from "react";
import { navigationKeywords } from "../../assets/navigationKeywords";
import { DragDropContext } from "react-dnd";
import HTML5Backend from "react-dnd-html5-backend";

import SectionSidebar from "./sectionSidebar/SectionSidebarContainer";
import LayoutToolbar from "./sectionToolbar/layoutToolbar/LayoutToolbarContainer";
import BackgroundToolbar from "./sectionToolbar/backgroundToolbar/BackgroundToolbarContainer";
import MenuPreviewPage from "./mainAreaContainer/menuPreview/MenuPreviewPageContainer";
import MainAreaToolbar from "./mainAreaContainer/mainAreaToolbar/MainAreaToolbarContainer";
import SetupToolbar from "./sectionToolbar/setupToolbar/SetupToolbarContainer";

import "../../styles/editor/editor.css";
import FormatToolbar from "./sectionToolbar/formatToolbar/FormatToolbarContainer";
import ElementsToolbar from "./sectionToolbar/elementsToolbar/ElementsToolbarContainer";
import MainAreaMenuBar from "./mainAreaContainer/mainAreaMenuBar/MainAreaMenuBarContainer";
import TextToolbar from "./sectionToolbar/textToolbar/TextToolbarContainer";

class EditorPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mainAreaContainerHeight: 100,
      editorPageStyle: {
        height: "0px",
        width: "0px"
      },
      toolbarStyle: {
        width: "0px"
      },
      mainAreaStyle: {
        width: "0px"
      }
    };
  }

  componentWillMount() {
    this.updateMainAreaContainerHeight();
  }

  componentDidMount() {
    window.addEventListener("resize", this.updateMainAreaContainerHeight);
  }

  updateMainAreaContainerHeight = () => {
    const { editorPageStyle, toolbarStyle, mainAreaStyle } = this.state;

    const newEditorPageStyle = Object.assign({}, editorPageStyle, {
      height: window.innerHeight - 54 + "px",
      width: window.innerWidth + "px"
    });

    let toolbarWidth = window.innerWidth * 0.25 - 80;
    let mainAreaWidth = "0px";

    if (toolbarWidth < 174) {
      toolbarWidth = "174px";
      mainAreaWidth = window.innerWidth - 204 + "px";
    } else if (toolbarWidth > 324) {
      toolbarWidth = "324px";
      mainAreaWidth = window.innerWidth - 404 + "px";
    } else {
      mainAreaWidth = window.innerWidth - toolbarWidth - 80 + "px";
      toolbarWidth = toolbarWidth + "px";
    }
    const newToolbarStyle = Object.assign({}, toolbarStyle, {
      width: toolbarWidth
    });

    const newMainAreaStyle = Object.assign({}, mainAreaStyle, {
      width: mainAreaWidth
    });

    const mainAreaContainerHeight = window.innerHeight - 70 - 50;
    this.setState({
      mainAreaContainerHeight,
      editorPageStyle: newEditorPageStyle,
      toolbarStyle: newToolbarStyle,
      mainAreaStyle: newMainAreaStyle
    });
  };

  render() {
    return (
      <div
        style={this.state.editorPageStyle}
        className={
          "containerPage editorPage " +
          (this.props.navigation.currentPage === navigationKeywords.EDITOR
            ? ""
            : "inActive")
        }
      >
        <SectionSidebar />
        <div style={this.state.toolbarStyle} id="sectionToolbar">
          <LayoutToolbar />
          <FormatToolbar />
          <ElementsToolbar />
          <BackgroundToolbar />
          <SetupToolbar />
          <TextToolbar />
        </div>
        <div style={this.state.mainAreaStyle} id="mainArea">
          <div id="mainAreaMenuBar">
            <MainAreaMenuBar />
          </div>
          <div
            style={{ height: this.state.mainAreaContainerHeight + "px" }}
            id="mainAreaContainer"
          >
            <MenuPreviewPage />
            <MainAreaToolbar />
          </div>
        </div>
      </div>
    );
  }
}

export default DragDropContext(HTML5Backend)(EditorPage);
