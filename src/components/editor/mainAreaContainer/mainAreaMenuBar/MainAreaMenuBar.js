import React from "react";
import Slider from "rc-slider";
import { Icon } from "semantic-ui-react";
import { editorSubSections } from "../../../../assets/navigationKeywords";
import "rc-slider/assets/index.css";
import "../../../../styles/editor/mainAreaMenuBar.css";

const elementTypes = ["illustrations", "accents", "shapes", "stockPhotos"];

export default class MainAreaMenuBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
    console.log(this.props.sele);
  }

  componentWillReceiveProps(nextProps) {
    console.log(this.props.selectedItem);
  }

  changeOpacity = value => {
    const { selectedItem } = this.props;
    if (selectedItem.type) {
      const styleData = {
        type: selectedItem.type,
        styleType: "opacity",
        value: value / 100
      };
      this.props.styleElement(
        selectedItem.index,
        styleData,
        this.props.pageNumber - 1
      );
    }
  };

  render() {
    const selectedItemKeys = Object.keys(this.props.selectedItem);
    const { selectedItem } = this.props;
    let opacity = 0;
    let style = {};
    if (selectedItemKeys.length > 0) {
      if (elementTypes.indexOf(selectedItem.type) !== -1) {
        style = this.props.menuData.pageData[this.props.pageNumber - 1]
          .elements[selectedItem.type][selectedItem.index].style;
        opacity = style.opacity;
      }
    }
    return (
      <div className="mainAreaMenuBar">
        <div className="menubarItem flipButton" id="flipButton">
          <p>Flip</p>
          <div className="itemDivider" />
        </div>
        <div className="menubarItem opacity">
          <p>Opacity</p>
          <Slider
            onChange={this.changeOpacity}
            value={opacity * 100}
            min={0}
            max={100}
          />
          <input value={opacity * 100 + "%"} />
          <div className="itemDivider" />
        </div>

        {(this.props.subSection === editorSubSections.ELEMENTS_ACCENT ||
          this.props.subSection === editorSubSections.ELEMENTS_SHAPE) && (
          <div className="moreItem">
            <div className="menubarItem color">
              <p>Color</p>
              <div style={{ backgroundColor: "red" }} className="colorBox" />
              <div className="itemDivider" />
            </div>
            <div className="menubarItem thickness">
              <p>Border thickness (px)</p>
              <input value="80" />
              <div className="itemDivider" />
            </div>
            <div className="menubarItem borderColor">
              <p>Border color</p>
              <div style={{ backgroundColor: "red" }} className="colorBox" />
              <div className="itemDivider" />
            </div>
          </div>
        )}

        <div className="menubarItem delete">
          <Icon name="trash alternate outline" />
          <p>Delete</p>
        </div>
        <div className="menubarItem copy">
          <Icon name="copy outline" />
          <p>Copy</p>
          <div className="itemDivider" />
        </div>
        <div className="menubarItem arrange">
          <p>Arrange</p>
          <div className="itemDivider" />
        </div>
      </div>
    );
  }
}
