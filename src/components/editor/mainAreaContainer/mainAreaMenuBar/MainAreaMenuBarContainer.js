import { connect } from "react-redux";
import { styleElement } from "../../../../redux/actions";
import MainAreaMenuBar from "./MainAreaMenuBar";
// import Redux Actions with the following line
// import {} from  ''

const mapStateToProps = (state, ownProps) => {
  return Object.assign(
    {},
    {
      subSection: state.navigation.editorSubsection,
      selectedItem: state.editor.selectedItem,
      menuData: state.editor.menuData,
      pageNumber: state.editor.page
    }
  );
};

const mapDispatchToProps = dispatch => {
  return {
    styleElement: (index, styleData, pageNumber) =>
      dispatch(styleElement(index, styleData, pageNumber))
  };
};

const MainAreaMenuBarContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(MainAreaMenuBar);

export default MainAreaMenuBarContainer;
