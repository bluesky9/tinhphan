/* global $ */
import React from "react";
import "../../../../styles/editor/menuPreview.css";
import { editorSections } from "../../../../assets/navigationKeywords";

let mousePosition = [0, 0];

let delay = false;

const elementTypes = ["accents", "illustrations", "shapes", "stockPhotos"];

export default class MenuPreviewPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      previewStyle: {
        height: 0,
        width: "0px"
      },
      containerStyle: {
        height: "100%",
        width: "500px"
      },
      windowHeight: 0,
      selectedItem: {},
      testPage: [0],
      dragging: false,
      isResizing: false,
      direction: ""
    };
  }

  componentDidMount() {
    window.addEventListener("resize", () => this.updatePreviewStle());
    window.addEventListener("keypress", e => {
      if (e.keyCode === 13) {
        console.log(this.state);
      }
    });
    window.addEventListener("mousemove", e => {
      if (this.state.dragging) {
        this.dragItem(e);
      } else if (this.state.isResizing) {
        this.resizeElement(e, this.state.direction);
      }
    });
    window.addEventListener("mousedown", this.clearSelected);
    window.addEventListener("mouseup", this.stopDragging);

    this.updatePreviewStle(null, null, null, true);
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.menuData.layout.style !== this.props.menuData.layout.style ||
      nextProps.menuData.layout.value !== this.props.menuData.layout.value
    ) {
      this.updatePreviewStle(
        nextProps.menuData.layout,
        nextProps.zoomLevel,
        this.props.zoomLevel !== nextProps.zoomLevel,
        true
      );
    } else {
      this.updatePreviewStle(
        nextProps.menuData.layout,
        nextProps.zoomLevel,
        this.props.zoomLevel !== nextProps.zoomLevel,
        false
      );
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.menuData.textUpdate !== this.props.menuData.textUpdate) {
      console.log(prevProps.menuData.textUpdate);
      console.log(this.props.menuData.textUpdate);

      this.checkPages();
    }
  }

  checkPages = () => {
    const pageInnerWidth = $(".menuDesign").innerWidth();
    const pageScrollWidth = $(".menuDesign")[0].scrollWidth;
    console.log(pageInnerWidth, pageScrollWidth);

    let numberOfPage = this.props.menuData.numberOfPage;

    if (pageInnerWidth !== pageScrollWidth) {
      numberOfPage = Math.floor(pageScrollWidth / pageInnerWidth) + 1;
    }

    const testPage = [];
    for (var i = 0; i < numberOfPage; i++) {
      testPage.push(i);
    }

    this.setState({
      testPage
    });

    if (numberOfPage !== this.props.menuData.numberOfPage) {
      this.props.updateNumberOfPage(numberOfPage);
    }
  };

  stopDragging = e => {
    this.setState({ dragging: false, isResizing: false });
  };

  clearSelected = e => {
    const relatedElements = document.elementsFromPoint(e.clientX, e.clientY);

    const selectedElement = relatedElements.find(a => {
      return $(a).hasClass("elements");
    });
    mousePosition[0] = e.clientX;
    mousePosition[1] = e.clientY;
    if (selectedElement) {
      const classes = $(selectedElement)[0].className.split(" ");
      const selectedElementData = {
        type: classes[1],
        index: Number(classes[2].split(".")[1])
      };
      this.props.selectItem(selectedElementData);
      this.setState({ dragging: true });
    } else {
      const selectedVertice = relatedElements.find(a =>
        $(a).hasClass("vertices")
      );
      if (selectedVertice) {
        const resizeDirection = $(selectedVertice)[0].className.split(" ")[1];
        this.setState({ isResizing: true, direction: resizeDirection });
        console.log(selectedVertice);
      } else {
        const toolbarExisted = relatedElements.find(
          a => $(a).attr("id") === "sectionToolbar"
        );
        if (toolbarExisted) {
          return;
        } else {
          if (this.props.selectedItem.type) {
            e.stopPropagation();

            const newSelectedElements = {};
            this.props.selectItem(newSelectedElements);
          }
        }
      }
    }
  };

  updatePreviewStle = (
    layoutData,
    nextZoomLevel,
    willUpdateOverflow,
    initialMenu
  ) => {
    const sizeValue = layoutData
      ? layoutData.sizeValue
      : this.props.menuData.layout.sizeValue;
    let zoomLevel = nextZoomLevel ? nextZoomLevel : this.props.zoomLevel;

    const availableHeight = window.innerHeight - 54 - 50 - 100;
    const availableWidth =
      window.innerWidth - $("#sectionToolbar").width() - 80 - 120 - 200;
    // const previewHeight = 0.95 * availableHeight;
    let previewHeight, previewWidth;

    previewWidth = sizeValue[0] * 10;
    previewHeight = sizeValue[1] * 10;
    if (initialMenu === true) {
      console.log("reset zoom level");

      if (sizeValue[0] > sizeValue[1] && initialMenu == true) {
        // previewWidth = availableWidth; // previewHeight = (previewWidth * sizeValue[1]) / sizeValue[0];
        zoomLevel = availableWidth / previewWidth;
        this.props.updateZoomLevel(zoomLevel);
      } else {
        zoomLevel = availableHeight / previewHeight;
        this.props.updateZoomLevel(zoomLevel);
      }
    }

    const previewStyle = this.state.previewStyle;
    const containerStyle = this.state.containerStyle;
    const containerWidth = window.innerWidth - 404 - 120;
    const windowHeight = window.innerHeight;

    const containerInnerWidth = $("#menuPreviewPageContainer").innerWidth();
    const containerScrollWidth = $("#menuPreviewPageContainer")[0].scrollWidth;
    const containerInnerHeight = $("#menuPreviewPageContainer").innerHeight();
    const containerScrollHeight = $("#menuPreviewPageContainer")[0]
      .scrollHeight;

    this.setState({
      previewStyle: Object.assign({}, previewStyle, {
        height: previewHeight + "px",
        width: previewWidth + "px",
        transform: `scale(${zoomLevel})`,
        marginLeft:
          containerInnerWidth === containerScrollWidth
            ? -0.5 * (previewWidth * zoomLevel) + "px"
            : 0,
        left: containerInnerWidth === containerScrollWidth ? "50%" : 0,
        marginTop:
          containerInnerHeight === containerScrollHeight ||
            containerInnerHeight - containerScrollHeight === 10
            ? -0.5 * (previewHeight * zoomLevel) + "px"
            : 0,
        top:
          containerInnerHeight === containerScrollHeight ||
            containerInnerHeight - containerScrollHeight === 10
            ? "50%"
            : 0
        // top: (availableHeight * 0.025) / 2 + "px"
      }),
      containerStyle: Object.assign({}, containerStyle, {
        height: window.innerHeight - 120 + "px",
        width: containerWidth + "px",
        overflow: "hidden"
      }),
      windowHeight: windowHeight
    });
    var parent = $("#menuPreviewPageContainer");
    /* if (previewHeight * zoomLevel < availableHeight) {
      parent.css("display", "table-cell");
    } else {
      parent.css("display", "block");
    } */

    if (willUpdateOverflow) {
      setTimeout(() => {
        this.updateOverflowStatus();
      }, 10);
    }
  };

  updateOverflowStatus = () => {
    this.setState({
      containerStyle: Object.assign({}, this.state.containerStyle, {
        overflow: "auto"
      })
    });
  };

  selectElement = (e, elementType, index) => {
    // Select Item for Dragging, Change Size
    if (this.props.navigation.editorSection === "element") {
      e.stopPropagation();
      const newSelectedElements = {
        type: elementType,
        index
      };

      this.props.selectItem(newSelectedElements);
    }
  };

  startDragging = e => {
    // Start change Position
    mousePosition[0] = e.clientX;
    mousePosition[1] = e.clientY;
    console.log(mousePosition);
  };

  dragItem = e => {
    const { selectedItem } = this.props;
    if (!selectedItem.type) {
      return;
    }
    if (delay || this.state.dragging === false) {
      return;
    }
    this.props.moveElement(
      selectedItem.type,
      selectedItem.index,
      (e.clientX - mousePosition[0]) / this.props.zoomLevel,
      (e.clientY - mousePosition[1]) / this.props.zoomLevel,
      this.props.editorPagination
    );
    mousePosition[0] = e.clientX;
    mousePosition[1] = e.clientY;
    delay = true;
    setTimeout(() => {
      delay = false;
    }, 100);
  };

  setDragImage = e => {
    // Handle changing Element Position
    this.dragImg = new Image(0, 0);
    this.dragImg.src =
      "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
    e.dataTransfer.setDragImage(this.dragImg, 0, 0);
  };

  resizeElement = (e, direction) => {
    e.stopPropagation();
    const { selectedItem } = this.props;
    if (delay) {
      return;
    }

    const diffX = (e.clientX - mousePosition[0]) / this.props.zoomLevel;
    const diffY = (e.clientY - mousePosition[1]) / this.props.zoomLevel;

    switch (direction) {
      case "topLeft":
        this.props.resizeElement(
          selectedItem.type,
          selectedItem.index,
          [-1 * diffX, -1 * diffY, diffX, diffY],
          this.props.editorPagination
        );
        break;
      case "topMiddle":
        this.props.resizeElement(selectedItem.type, selectedItem.index, [
          0,
          -1 * diffY,
          0,
          diffY
        ]);
        break;
      case "topRight":
        this.props.resizeElement(
          selectedItem.type,
          selectedItem.index,
          [1 * diffX, -1 * diffY, 0, diffY],
          this.props.editorPagination
        );
        break;
      case "middleLeft":
        this.props.resizeElement(
          selectedItem.type,
          selectedItem.index,
          [-1 * diffX, 0, diffX, 0],
          this.props.editorPagination
        );
        break;
      case "middleRight":
        this.props.resizeElement(
          selectedItem.type,
          selectedItem.index,
          [1 * diffX, 0, 0, 0],
          this.props.editorPagination
        );
        break;
      case "bottomLeft":
        this.props.resizeElement(
          selectedItem.type,
          selectedItem.index,
          [-1 * diffX, 1 * diffY, 1 * diffX, 0],
          this.props.editorPagination
        );
        break;
      case "bottomMiddle":
        this.props.resizeElement(
          selectedItem.type,
          selectedItem.index,
          [0, 1 * diffY, 0, 0],
          this.props.editorPagination
        );
        break;
      default:
        this.props.resizeElement(
          selectedItem.type,
          selectedItem.index,
          [diffX, diffY, 0, 0],
          this.props.editorPagination
        );
        break;
    }

    mousePosition[0] = e.clientX;
    mousePosition[1] = e.clientY;
    delay = true;
    setTimeout(() => {
      delay = false;
    }, 100);
  };

  selectTextSection = sectionIndex => {
    const selectedItemData = {
      type: "textSection",
      index: sectionIndex,
      pageNumber: this.props.editorPagination - 1
    };

    this.props.selectItem(selectedItemData);
  };

  render() {
    const { selectedItem, templateInfo, menuData } = this.props;
    return (
      <div style={this.state.containerStyle} id="menuPreviewPageContainer">
        {this.state.testPage.map((pageNumber, index) => {
          return (
            <div
              style={Object.assign({}, this.state.previewStyle, {
                visibility:
                  this.props.editorPagination !== index + 1
                    ? "hidden"
                    : "visible",
                backgroundColor: menuData.background.color || "",
              })}
              className={
                "menuPreviewPage " +
                (this.state.windowHeight <= 900 ? "" : "toolbarExpanded")
              }
            >
              {(this.props.menuData.layout.style === "booklet" ||
                (this.props.menuData.layout.style === "folded" &&
                  (this.props.menuData.layout.value === "a4" ||
                    this.props.menuData.layout.value === "letter"))) && (
                  <div id="bookletSeparator" />
                )}
              {this.props.menuData.layout.style === "folded" &&
                (this.props.menuData.layout.value === "newspaper" ||
                  this.props.menuData.layout.value === "legal") && (
                  <div id="foldedSeparators">
                    <div className="first separator" />
                    <div className="second separator" />
                  </div>
                )}
              <div className="menuDesign">
                <div
                  className="menuText"
                  style={{
                    marginLeft:
                      this.state.previewStyle.width.split("px")[0] *
                      (index * -1) +
                      index * -9 +
                      "px"
                  }}
                >
                  {this.props.menuData.text.map((sectionData, index) => (
                    <MenuSection
                      sectionFormat={sectionData.formatData}
                      editorSection={this.props.navigation.editorSection}
                      sectionItems={sectionData.data}
                      sectionIndex={index}
                      key={index}
                      selectedSection={
                        selectedItem.type === "textSection"
                          ? selectedItem
                          : null
                      }
                      selectTextSection={this.selectTextSection}
                    />
                  ))}
                </div>
                {elementTypes.map((types, index1) => {
                  return this.props.menuData.pageData[
                    this.props.editorPagination - 1
                  ].elements[types].map((elementData, index) => (
                    <ElementWrapper
                      selectedItem={selectedItem}
                      key={index}
                      style={elementData.style}
                      onClick={this.selectElement}
                      index={index}
                      type={types}
                      startDragging={this.startDragging}
                      dragItem={this.dragItem}
                      setDragImage={this.setDragImage}
                      updateOverflowStatus={this.updateOverflowStatus}
                      resizeElement={this.resizeElement}
                    >
                      <img
                        className={"elements " + types + ` index.${index}`}
                        src={elementData.imageURL}
                        alt={elementData.value}
                      />
                    </ElementWrapper>
                  ));
                })}

                <img src={`${menuData.background.stockPhotos || templateInfo.imageURL || "/templateImages/demoMenuPreview.png"}`} alt="Menu" />
              </div>
            </div>
          );
        })}
      </div>
    );
  }
}

const MenuSection = props => {
  const sectionStyle = {};
  sectionStyle.columnFill = "auto";

  switch (props.sectionFormat.columns) {
    case "oneColumn":
      sectionStyle.columns = 1;
      break;
    case "twoColumns":
      sectionStyle.columns = 2;
      break;
    case "threeColumns":
      sectionStyle.columns = 3;
      break;
    case "twoSmallLeft":
      sectionStyle.columns = 2;
      break;
    case "twoSmallRight":
      sectionStyle.columns = 2;
      break;
    default:
      break;
  }

  return (
    <div
      style={sectionStyle}
      className={
        "menuSection " +
        (props.editorSection === editorSections.TEXT ? "selectable" : "") +
        (props.selectedSection &&
          props.selectedSection.index === props.sectionIndex
          ? " selected"
          : "")
      }
      onClick={() => props.selectTextSection(props.sectionIndex)}
    >
      {props.sectionItems.map((itemData, index) => (
        <div key={index} className="menuItem">
          <p className="name">{itemData.name}</p>
          <p className="description">{itemData.description}</p>
          <p className="price">{itemData.price}</p>
        </div>
      ))}
    </div>
  );
};

const ElementWrapper = props => {
  return (
    <div
      onMouseDown={props.startDragging}
      onDrag={props.dragItem}
      onDragEnd={props.updateOverflowStatus}
      onDragStart={props.setDragImage}
      style={props.style}
      onClick={e => props.onClick(e, props.type, props.index)}
      className={
        "menuElements " +
        (props.selectedItem.type === props.type &&
          props.selectedItem.index === props.index
          ? "selected"
          : "")
      }
    >
      <img
        src="/basicImages/dragPoint.png"
        onDrag={e => props.resizeElement(e, "topLeft")}
        onDragStart={props.startDragging}
        className="vertices topLeft"
      />
      <img
        src="/basicImages/dragPoint.png"
        onDrag={e => props.resizeElement(e, "topMiddle")}
        onDragStart={props.startDragging}
        className="vertices topMiddle"
      />
      <img
        src="/basicImages/dragPoint.png"
        onDrag={e => props.resizeElement(e, "topRight")}
        onDragStart={props.startDragging}
        className="vertices topRight"
      />
      <img
        src="/basicImages/dragPoint.png"
        onDrag={e => props.resizeElement(e, "middleLeft")}
        onDragStart={props.startDragging}
        className="vertices middleLeft"
      />
      <img
        src="/basicImages/dragPoint.png"
        onDrag={e => props.resizeElement(e, "middleRight")}
        onDragStart={props.startDragging}
        className="vertices middleRight"
      />
      <img
        src="/basicImages/dragPoint.png"
        onDrag={e => props.resizeElement(e, "bottomLeft")}
        onDragStart={props.startDragging}
        className="vertices bottomLeft"
      />
      <img
        src="/basicImages/dragPoint.png"
        onDrag={e => props.resizeElement(e, "bottomMiddle")}
        onDragStart={props.startDragging}
        className="vertices bottomMiddle"
      />
      <img
        src="/basicImages/dragPoint.png"
        onDrag={e => props.resizeElement(e, "bottomRight")}
        onDragStart={props.startDragging}
        className="vertices bottomRight"
      />
      {props.children}
    </div>
  );
};
