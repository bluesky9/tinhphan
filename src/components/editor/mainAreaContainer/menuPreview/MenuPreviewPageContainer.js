import { connect } from "react-redux";
import {
  moveElement,
  resizeElement,
  updateNumberOfPage,
  updateZoomLevel,
  selectItem
} from "../../../../redux/actions";
import MenuPreviewPage from "./MenuPreviewPage";
// import Redux Actions with the following line
// import {} from  ''

const mapStateToProps = (state, ownProps) => {
  return Object.assign(
    {},
    {
      menuData: state.editor.menuData,
      zoomLevel: state.editor.zoomLevel,
      editorPagination: state.editor.page,
      navigation: state.navigation,
      selectedItem: state.editor.selectedItem,
      templateInfo: state.editor.templateInfo
    }
  );
};

const mapDispatchToProps = dispatch => {
  return {
    moveElement: (type, index, changeX, changeY, pageNumber) =>
      dispatch(moveElement(type, index, changeX, changeY, pageNumber)),
    resizeElement: (type, index, changes, pageNumber) =>
      dispatch(resizeElement(type, index, changes, pageNumber)),
    updateNumberOfPage: numberOfPage =>
      dispatch(updateNumberOfPage(numberOfPage)),
    updateZoomLevel: zoomLevel => dispatch(updateZoomLevel(zoomLevel)),
    selectItem: itemData => dispatch(selectItem(itemData))
  };
};

const MenuPreviewPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(MenuPreviewPage);

export default MenuPreviewPageContainer;
