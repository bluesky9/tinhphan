import { connect } from "react-redux";
import { zoomEditor, changeEditorPage } from "../../../../redux/actions";
import MainAreaToolbar from "./MainAreaToolbar";
// import Redux Actions with the following line
// import {} from  ''

const mapStateToProps = (state, ownProps) => {
  return Object.assign(
    {},
    {
      editorPagination: state.editor.page
    }
  );
};

const mapDispatchToProps = dispatch => {
  return {
    zoomEditor: isIncrement => dispatch(zoomEditor(isIncrement)),
    changeEditorPage: isIncrement => dispatch(changeEditorPage(isIncrement))
  };
};

const MainAreaToolbarContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(MainAreaToolbar);

export default MainAreaToolbarContainer;
