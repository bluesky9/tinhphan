import React from "react";
import { Icon } from "semantic-ui-react";
import "../../../../styles/editor/mainAreaToolbar.css";

export default class MainAreaToolbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      toolbarStyle: {
        height: 0
      },
      windowHeight: 0
    };
  }

  componentDidMount() {
    window.addEventListener("resize", this.updatePreviewStle);

    this.updatePreviewStle();
  }

  updatePreviewStle = () => {
    // Update Styling when user resize browser
    const toolbarContainerHeight = window.innerHeight - 70 - 50;
    const toolbarHeight = 0.95 * toolbarContainerHeight;
    const toolbarStyle = this.state.previewwStyle;
    const windowHeight = window.innerHeight;

    this.setState({
      toolbarStyle: Object.assign({}, toolbarStyle, {
        height: toolbarHeight - (toolbarContainerHeight - toolbarHeight) + "px",
        top: (toolbarContainerHeight - toolbarHeight) / 2 + 120 + "px",
        right: "50px",
        position: "fixed"
      }),
      windowHeight: windowHeight
    });
  };

  holdZoomEditor = isIncrement => {
    this.zoomEditorInterval = setInterval(() => {
      this.props.zoomEditor(isIncrement);
    }, 200);
  };

  releaseZoomEditor = () => {
    clearInterval(this.zoomEditorInterval);
  };

  render() {
    return (
      <div
        style={this.state.toolbarStyle}
        className={
          "mainAreaToolbar " +
          (this.state.windowHeight <= 900 ? "" : "expanded")
        }
      >
        <div
          onClick={() => this.props.zoomEditor(true)}
          onMouseDown={() => this.holdZoomEditor(true)}
          onMouseOut={this.releaseZoomEditor}
          onMouseUp={this.releaseZoomEditor}
          className="toolbarButton"
          id="zoomInBtn"
        >
          <Icon name="zoom in" />
        </div>
        <div
          onClick={() => this.props.zoomEditor(false)}
          onMouseDown={() => this.holdZoomEditor(false)}
          onMouseOut={this.releaseZoomEditor}
          onMouseUp={this.releaseZoomEditor}
          className="toolbarButton"
          id="zoomOutBtn"
        >
          <Icon name="zoom out" />
        </div>
        <div id="pagination">
          <img
            onClick={() => this.props.changeEditorPage(true)}
            src="/basicImages/upBtn.png"
            alt="Up Button"
            id="upBtn"
          />
          <div id="pageNumber">{this.props.editorPagination}</div>
          <img
            onClick={() => this.props.changeEditorPage(false)}
            src="/basicImages/downBtn.png"
            alt="Down Button"
            id="downBtn"
          />
        </div>
        <img src="/basicImages/previewBtn.png" alt="Preview" id="previewBtn" />
        <img src="/basicImages/addBtn.png" alt="Add New Page" id="addBtn" />
        <img src="/basicImages/copyBtn.png" alt="Copy" id="copyBtn" />
        <img src="/basicImages/deleteBtn.png" alt="Delete" id="deleteBtn" />
      </div>
    );
  }
}
