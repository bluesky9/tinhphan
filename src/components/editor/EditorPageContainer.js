import { connect } from "react-redux";
import EditorPage from "./EditorPage";
// import Redux Actions with the following line
// import {} from  ''

const mapStateToProps = (state, ownProps) => {
  return Object.assign(
    {},
    {
      navigation: state.navigation,
    }
  );
};

const mapDispatchToProps = dispatch => {
  return {
    // Dispatch here
  };
};

const EditorPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(EditorPage);

export default EditorPageContainer;
