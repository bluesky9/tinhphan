import { connect } from "react-redux";
import FormatToolbar from "./FormatToolbar";
import { formatTextSection } from "../../../../redux/actions";
// import Redux Actions with the following line
// import {} from  ''

const mapStateToProps = (state, ownProps) => {
  return Object.assign(
    {},
    {
      navigation: state.navigation,
      menuData: state.editor.menuData,
      selectedItem: state.editor.selectedItem
    }
  );
};

const mapDispatchToProps = dispatch => {
  return {
    formatTextSection: (sectionIndex, formatData) =>
      dispatch(formatTextSection(sectionIndex, formatData))
  };
};

const FormatToolbarContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(FormatToolbar);

export default FormatToolbarContainer;
