export const formatData = [
  {
    name: "Section Columns",
    value: "sectionColumns",
    data: [
      {
        value: "oneColumn",
        imageURL: "/basicImages/formatColumn1.png"
      },
      {
        value: "twoColumns",
        imageURL: "/basicImages/formatColumn2.png"
      },
      {
        value: "threeColumns",
        imageURL: "/basicImages/formatColumn3.png"
      },
      {
        value: "twoSmallLeft",
        imageURL: "/basicImages/formatColumn4.png"
      },
      {
        value: "twoSmallRight",
        imageURL: "/basicImages/formatColumn5.png"
      }
    ]
  },
  {
    name: "Highlight",
    value: "highlight",
    data: [
      {
        value: "single",
        imageURL: "/basicImages/singleLine.png"
      },
      {
        value: "dottedLine",
        imageURL: "/basicImages/dottedLine.png"
      },
      {
        value: "single2",
        imageURL: "/basicImages/singleLine.png"
      },
      {
        value: "smallDotted",
        imageURL: "/basicImages/smallDottedLine.png"
      }
    ]
  },
  {
    name: "Price Style",
    value: "priceStyle",
    data: {
      singlePrice: [
        {
          name: "Price after description",
          value: "priceAfterDes",
          imageURL: "/basicImages/priceAfterDes.png"
        },
        {
          name: "Price after name",
          value: "priceAfterName",
          imageURL: "/basicImages/priceAfterName.png"
        },
        {
          name: "All same line",
          value: "inlinePrice",
          imageURL: "/basicImages/inlinePrice.png"
        }
      ],
      multiPrice: [
        {
          name: "Price after name",
          value: "multiPriceAfterName",
          imageURL: "/basicImages/multiPriceAfterName.png"
        },
        {
          name: "All same line",
          value: "multiPriceInline",
          imageURL: "/basicImages/multiPriceInline.png"
        }
      ]
    }
  }
];
