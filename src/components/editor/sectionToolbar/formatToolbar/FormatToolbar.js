import React from "react";
import { editorSections } from "../../../../assets/navigationKeywords";
import "../../../../styles/editor/formatToolbar.css";
import { formatData } from "./formatData";

export default class FormatToolbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openedMenu: []
    };
  }

  formatTitleClicked = styleName => {
    // Open or Close toolbar section when click
    const openedMenu = this.state.openedMenu;
    const styleNamePos = openedMenu.indexOf(styleName);
    if (styleNamePos === -1) {
      openedMenu.push(styleName);
    } else {
      openedMenu.splice(styleNamePos, 1);
    }

    this.setState({ openedMenu });
  };

  setSectionColumn = (e, columnValue) => {
    e.stopPropagation();

    const { selectedItem } = this.props;
    console.log(selectedItem);

    if (selectedItem && selectedItem.type === "textSection") {
      const formatData = {
        type: "columns",
        value: columnValue
      };
      const sectionIndex = selectedItem.index;

      this.props.formatTextSection(sectionIndex, formatData);
    }
  };

  render() {
    return (
      <div
        className={
          "toolbar formatToolbar " +
          (this.props.navigation.editorSection === editorSections.FORMAT
            ? ""
            : "inActive")
        }
      >
        <p id="toolbarTitle">FORMAT</p>
        {formatData.map((menuData, index) => (
          <div className="toolbarSection" key={index}>
            <div
              onClick={() => this.formatTitleClicked(menuData.value)}
              className={
                "formatTitle " +
                (this.state.openedMenu.indexOf(menuData.value) > -1
                  ? "active"
                  : "")
              }
            >
              <img
                className={
                  "expandIcon " +
                  (this.state.openedMenu.indexOf(menuData.value) > -1
                    ? "inActive"
                    : "")
                }
                src="/basicImages/smallArrow.png"
                alt="Expand Menu"
              />
              <img
                className={
                  "expandIcon " +
                  `${menuData.value} ` +
                  (this.state.openedMenu.indexOf(menuData.value) > -1
                    ? ""
                    : "inActive")
                }
                src="/basicImages/activeSmallArrow.png"
                alt="Expand Menu"
              />

              <p>{menuData.name}</p>
            </div>
            <div
              className={
                "formatContent " +
                (this.state.openedMenu.indexOf(menuData.value) > -1
                  ? "expanded"
                  : "")
              }
            >
              {menuData.value !== "priceStyle" ? (
                <div>
                  <p>
                    {menuData.value === "sectionColumns"
                      ? "Apply to current section"
                      : "Select line style to add highlight"}
                  </p>
                  <div className="menuOptions">
                    {menuData.data.map((menuOptions, index2) => {
                      if (menuData.value === "sectionColumns") {
                        return (
                          <img
                            onClick={e =>
                              this.setSectionColumn(e, menuOptions.value)
                            }
                            className="sectionColumnsItem"
                            src={menuOptions.imageURL}
                            alt={menuOptions.value}
                          />
                        );
                      } else {
                        return (
                          <div className="highlightItem">
                            <img
                              src={menuOptions.imageURL}
                              alt={menuOptions.value}
                            />
                          </div>
                        );
                      }
                    })}
                  </div>
                </div>
              ) : (
                <PriceStyle menuData={menuData} />
              )}
            </div>
          </div>
        ))}
      </div>
    );
  }
}

const PriceStyle = ({ menuData }) => (
  <div className="menuOptions priceStyle">
    {menuData.data.singlePrice.map((menuOption, index) => (
      <div className="menuItem">
        <img src={menuOption.imageURL} alt={menuOption.name} />{" "}
        <p>{menuOption.name}</p>
      </div>
    ))}
    <button id="singlePriceApply">Apply to all</button>
    {menuData.data.multiPrice.map((menuOption, index) => (
      <div className="menuItem">
        <img src={menuOption.imageURL} alt={menuOption.name} />{" "}
        <p>{menuOption.name}</p>
      </div>
    ))}
    <button id="multiPriceApply">Apply to all</button>
  </div>
);
