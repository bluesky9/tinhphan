import { connect } from "react-redux";
import { updateMenuLayout } from "../../../../redux/actions";
import LayoutToolbar from "./LayoutToolbar";
import { layoutData } from "./layoutData";
// import Redux Actions with the following line
// import {} from  ''

const mapStateToProps = (state, ownProps) => {
  return Object.assign(
    {},
    {
      navigation: state.navigation
    }
  );
};

const mapDispatchToProps = dispatch => {
  return {
    updateMenuLayout: layoutData => dispatch(updateMenuLayout(layoutData))
  };
};

const LayoutToolbarContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(LayoutToolbar);

export default LayoutToolbarContainer;
