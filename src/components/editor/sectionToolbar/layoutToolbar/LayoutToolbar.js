import React from "react";
import { editorSections } from "../../../../assets/navigationKeywords";
import { layoutData } from "./layoutData";
import "../../../../styles/editor/layoutToolbar.css";

export default class LayoutToolbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openedMenu: [],
      styleItemValue: ""
    };
  }

  styleTitleClicked = styleName => {
    const openedMenu = this.state.openedMenu;
    const styleNamePos = openedMenu.indexOf(styleName);
    if (styleNamePos === -1) {
      openedMenu.push(styleName);
    } else {
      openedMenu.splice(styleNamePos, 1);
    }

    this.setState({ openedMenu });
  };

  styleItemSelected = (layoutStyle, layoutInfo, layoutSizeValue) => {
    let styleItemValue = this.state.styleItemValue;
    styleItemValue = layoutStyle + "__" + layoutInfo;

    this.setState({ styleItemValue });

    this.props.updateMenuLayout({
      style: layoutStyle,
      value: layoutInfo,
      sizeValue: layoutSizeValue
    });
  };

  render() {
    const selectedStyleItem = this.state.styleItemValue.split("__");
    return (
      <div
        className={
          "toolbar layoutToolbar " +
          (this.props.navigation.editorSection === editorSections.LAYOUT
            ? ""
            : "inActive")
        }
      >
        <p id="toolbarTitle">LAYOUT</p>
        <p id="toolbarDescription">Select page format</p>

        {layoutData.map((layoutStyle, index) => (
          <div key={index} className="toolbarSection">
            <div
              onClick={() => this.styleTitleClicked(layoutStyle.value)}
              className={
                "styleTitle " +
                (this.state.openedMenu.indexOf(layoutStyle.value) > -1
                  ? "active"
                  : "")
              }
            >
              <img
                className={
                  "expandIcon " +
                  (this.state.openedMenu.indexOf(layoutStyle.value) > -1
                    ? "inActive"
                    : "")
                }
                src="/basicImages/smallArrow.png"
                alt="Expand Menu"
              />
              <img
                className={
                  "expandIcon " +
                  `${layoutStyle.value} ` +
                  (this.state.openedMenu.indexOf(layoutStyle.value) > -1
                    ? ""
                    : "inActive")
                }
                src="/basicImages/activeSmallArrow.png"
                alt="Expand Menu"
              />

              <p>{layoutStyle.name}</p>
              <div className="styleIcon">
                <img src={layoutStyle.iconImage} alt={layoutStyle.name} />
              </div>
            </div>
            <div
              className={
                "styleContent " +
                (this.state.openedMenu.indexOf(layoutStyle.value) > -1
                  ? "expanded"
                  : "")
              }
            >
              {layoutStyle.data.map((layoutInfo, index2) => (
                <div
                  onClick={() =>
                    this.styleItemSelected(
                      layoutStyle.value,
                      layoutInfo.value,
                      layoutInfo.sizeValue
                    )
                  }
                  className={
                    "layoutStyleItem " +
                    (selectedStyleItem[0] === layoutStyle.value &&
                    selectedStyleItem[1] === layoutInfo.value
                      ? "active"
                      : "")
                  }
                  key={index2}
                >
                  {layoutInfo.name}
                  <img
                    className={
                      selectedStyleItem[0] === layoutStyle.value &&
                      selectedStyleItem[1] === layoutInfo.value
                        ? "active"
                        : ""
                    }
                    src="/basicImages/menuItemClick.png"
                    alt="Selected Layout"
                  />
                </div>
              ))}
            </div>
          </div>
        ))}
      </div>
    );
  }
}
