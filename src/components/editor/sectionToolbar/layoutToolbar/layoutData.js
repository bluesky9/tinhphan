export const layoutData = [
  {
    name: "Flat Landscape",
    value: "flatLandscape",
    iconImage: "/basicImages/landscapeIcon.png",
    data: [
      {
        name: "Newspaper: 43.2 x 27.9 (17 x 11)",
        value: "newspaper",
        sizeValue: [43.2, 27.9]
      },
      {
        name: "Legal: 35.6 x 21.6 (14 x 8.5)",
        value: "legal",
        sizeValue: [35.6, 21.6]
      },
      {
        name: "A4: 29.7 x 21.0 (11.7 x 8.3)",
        value: "a4",
        sizeValue: [29.7, 21]
      },
      {
        name: "Letter: 27.9 x 21.6 (11 x 8.5)",
        value: "letter",
        sizeValue: [27.9, 21.6]
      },
      {
        name: "A5: 21.0 x 14.8 (8.3 x 5.8)",
        value: "a5",
        sizeValue: [21, 14.8]
      }
    ]
  },
  {
    name: "Flat Portrait",
    value: "flatPortrait",
    iconImage: "/basicImages/portraitIcon.png",
    data: [
      {
        name: "Newspaper: 27.9 x 43.2 (11 x 17)",
        value: "newspaper",
        sizeValue: [27.9, 43.2]
      },
      {
        name: "Legal: 21.6 x 35.6 (8.5 x 14)",
        value: "legal",
        sizeValue: [21.6, 25.6]
      },
      {
        name: "A4: 21.0 x 29.7 (8.3 x 11.7)",
        value: "a4",
        sizeValue: [21, 29.7]
      },
      {
        name: "Letter: 21.6 x 27.9 (8.5 x 11)",
        value: "letter",
        sizeValue: [21.6, 27.9]
      },
      {
        name: "A5: 14.8 x 21.0 (5.8 x 8.3)",
        value: "a5",
        sizeValue: [14.8, 21]
      }
    ]
  },
  {
    name: "Booklet",
    value: "booklet",
    iconImage: "/basicImages/bookletIcon.png",
    data: [
      {
        name: "A4 landscape: 29.7 x 21.0 (11.7 x 8.3)",
        value: "a4l",
        sizeValue: [29.7, 21]
      },
      {
        name: "A4 portrait: 21.0 x 29.7 (8.3 x 11.7)",
        value: "a4p",
        sizeValue: [21, 29.7]
      },
      {
        name: "A5 landscape: 21.0 x 14.8 (8.3 x 5.8)",
        value: "a5l",
        sizeValue: [21, 14.8]
      },
      {
        name: "A5 portrait: 14.8 x 21.0 (5.8 x 8.3)",
        value: "a5p",
        sizeValue: [14.8, 21]
      }
    ]
  },
  {
    name: "Folded",
    value: "folded",
    iconImage: "/basicImages/foldedIcon.png",
    data: [
      {
        name: "A4 Bifold: 29.7 x 21.0 (11.7 x 8.3)",
        value: "a4",
        sizeValue: [29.7, 21]
      },
      {
        name: "Letter Bifold: 27.9 x 21.6 (11 x 8.5)",
        value: "letter",
        sizeValue: [27.9, 21.6]
      },
      {
        name: "Newspaper Trifold: 43.2 x 27.9 (17 x 11)",
        value: "newspaper",
        sizeValue: [43.2, 27.9]
      },
      {
        name: "Legal Trifold: 35.6 x 21.6 (14 x 8.5)",
        value: "legal",
        sizeValue: [35.6, 21.6]
      }
    ]
  }
];
