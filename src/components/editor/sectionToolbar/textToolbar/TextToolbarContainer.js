import { connect } from "react-redux";
import {
  addNewMenuSection,
  addNewMenuItem,
  editSecitonTitle,
  editMenuItemText
} from "../../../../redux/actions";
import TextToolbar from "./TextToolbar";
// import Redux Actions with the following line
// import {} from  ''

const mapStateToProps = (state, ownProps) => {
  return Object.assign(
    {},
    {
      navigation: state.navigation,
      text: state.editor.menuData.text
    }
  );
};

const mapDispatchToProps = dispatch => {
  return {
    addSection: sectionData => dispatch(addNewMenuSection(sectionData)),
    addItem: sectionPos => dispatch(addNewMenuItem(sectionPos)),
    editSectionTitle: (sectionId, value) =>
      dispatch(editSecitonTitle(sectionId, value)),
    editMenuItemText: (itemId, type, value) =>
      dispatch(editMenuItemText(itemId, type, value))
  };
};

const TextToolbarContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(TextToolbar);

export default TextToolbarContainer;
