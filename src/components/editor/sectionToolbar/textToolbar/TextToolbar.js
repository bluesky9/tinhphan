import React from "react";
import { editorSections } from "../../../../assets/navigationKeywords";
import "../../../../styles/editor/textToolbar.css";
import { Input, Icon, TextArea } from "semantic-ui-react";

export default class TextToolbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openedSection: [],
      openedItem: [],
      editingSectionTitle: -1
    };
  }

  componentWillReceiveProps(nextProps) {
    console.log("run run");
  }

  componentDidMount() {
    window.addEventListener("keypress", e => {
      if (e.keyCode === 13) {
        this.handleEnter();
      }
    });
  }

  handleEnter = () => {
    if (this.state.editingSectionTitle !== -1) {
      this.setState({ editingSectionTitle: -1 });
    }
  };

  sectionTitleClicked = styleName => {
    // Open or Close toolbar section when click
    const openedSection = this.state.openedSection;
    const currentSection = openedSection.splice(0, 1);
    if (currentSection[0] !== styleName) {
      openedSection.push(styleName);
    }
    this.setState({ openedSection });
  };

  menuItemTitleClicked = menuItemId => {
    // Open or Close Menu Item for Editing
    const openedItem = this.state.openedItem;
    const currentItem = openedItem.splice(0, 1);
    if (currentItem[0] !== menuItemId) {
      openedItem.push(menuItemId);
    }
    this.setState({ openedItem });
  };

  startEditSectionTitle = (e, sectionId) => {
    e.stopPropagation();
    this.setState({ editingSectionTitle: sectionId });
  };

  render() {
    return (
      <div
        className={
          "toolbar textToolbar " +
          (this.props.navigation.editorSection === editorSections.TEXT
            ? ""
            : "inActive")
        }
      >
        <div id="headerButtons">
          <div id="headers">Headers</div>
          <div id="menuItems">Menu Item</div>
          <div id="footers">Footers</div>
        </div>
        <div className="textSearch">
          <Input icon placeholder="Search item ...">
            <input />
            <Icon name="search" />
          </Input>
        </div>
        <p id="toolbarTitle">
          MENU ITEMS
          <button
            onClick={e => {
              e.stopPropagation();
              this.props.addSection({ name: "New Section" });
            }}
            id="addSection"
          >
            Add section
          </button>
        </p>
        {this.props.text.map((sectionData, index) => (
          <div key={index} className="menuSection">
            <SectionTitle
              editingSectionTitle={this.state.editingSectionTitle}
              openedMenu={this.state.openedSection}
              value={index}
              name={sectionData.name}
              titleClicked={this.sectionTitleClicked}
              class="menuTitle"
              startEditSectionTitle={this.startEditSectionTitle}
              editSectionTitle={this.props.editSectionTitle}
            />
            <div
              className={
                "sectionContent " +
                (this.state.openedSection.indexOf(index) > -1 ? "expanded" : "")
              }
            >
              <div className="menuSectionToolbar">
                <button
                  onClick={() => this.props.addItem(index)}
                  className="addItem"
                >
                  Add item
                </button>
                <button className="addNote">Add Note</button>
              </div>
              {sectionData.data.map((menuItem, index2) => {
                const isActive =
                  this.state.openedItem.indexOf(`${index}.${index2}`) > -1;
                return (
                  <div className="menuItem ">
                    <SectionTitle
                      openedMenu={this.state.openedItem}
                      value={`${index}.${index2}`}
                      name={menuItem.name}
                      titleClicked={this.menuItemTitleClicked}
                      class="menuItemTitle menuTitle"
                      menuItem={true}
                    />
                    <div
                      className={
                        "menuItemContent " + (isActive ? "expanded" : "")
                      }
                    >
                      <TextArea
                        onChange={(e, data) =>
                          this.props.editMenuItemText(
                            `${index}.${index2}`,
                            "name",
                            data.value
                          )
                        }
                        value={menuItem.name}
                      />
                      <TextArea
                        onChange={(e, data) =>
                          this.props.editMenuItemText(
                            `${index}.${index2}`,
                            "description",
                            data.value
                          )
                        }
                        value={menuItem.description}
                      />
                      <Input
                        onChange={(e, data) =>
                          this.props.editMenuItemText(
                            `${index}.${index2}`,
                            "price",
                            data.value
                          )
                        }
                        type="number"
                        value={menuItem.price}
                      />
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        ))}
      </div>
    );
  }
}

const SectionTitle = props => {
  return (
    <div
      onClick={() => props.titleClicked(props.value)}
      className={
        `${props.class} ` +
        (props.openedMenu.indexOf(props.value) > -1 ? "active" : "")
      }
    >
      <img
        className={
          "expandIcon " +
          (props.openedMenu.indexOf(props.value) > -1 ? "inActive" : "")
        }
        src="/basicImages/smallArrow.png"
        alt="Expand Menu"
      />
      <img
        className={
          "expandIcon " +
          `${props.name} ` +
          (props.openedMenu.indexOf(props.value) > -1 ? "" : "inActive")
        }
        src="/basicImages/activeSmallArrow.png"
        alt="Expand Menu"
      />
      {props.editingSectionTitle === props.value ? (
        <Input
          value={props.name}
          name={props.value}
          onChange={(e, data) => {
            props.editSectionTitle(props.value, data.value);
          }}
        />
      ) : (
        <p
          onClick={e => {
            if (!props.menuItem) {
              props.startEditSectionTitle(e, props.value);
            }
          }}
        >
          {props.name}
        </p>
      )}
    </div>
  );
};
