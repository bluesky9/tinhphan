/* global $ */
import React from "react";
import {
  editorSections,
  editorSubSections
} from "../../../../assets/navigationKeywords";
import "../../../../styles/editor/elementsToolbar.css";
// import { elementsData } from "./elementsData";
import { Input, Icon } from "semantic-ui-react";
import { searchImages } from "pixabay-api";
export default class ElementsToolbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openedMenu: [],
      elementsData: [
        {
          name: "Accents",
          value: "accents",
          description: "Select accents",
          data: [
            {
              value: "accentBlank",
              imageURL: "/basicImages/elements/accentBlank.png"
            },
            {
              value: "accentTop",
              imageURL: "/basicImages/elements/accentTop.png"
            },
            {
              value: "accentHot",
              imageURL: "/basicImages/elements/accentHot.png"
            },
            {
              value: "accentNew",
              imageURL: "/basicImages/elements/accentNew.png"
            },
            {
              value: "accentSpec",
              imageURL: "/basicImages/elements/accentSpec.png"
            },
            {
              value: "accentMenu",
              imageURL: "/basicImages/elements/accentMenu.png"
            },
            {
              value: "accentVetageble",
              imageURL: "/basicImages/elements/accentVegetable.png"
            },
            {
              value: "accentNonVetageble",
              imageURL: "/basicImages/elements/accentNonVegetable.png"
            },
            {
              value: "accentSpicy1",
              imageURL: "/basicImages/elements/accentSpicy1.png"
            },
            {
              value: "accentSpicy2",
              imageURL: "/basicImages/elements/accentSpicy2.png"
            },
            {
              value: "accentSpicy3",
              imageURL: "/basicImages/elements/accentSpicy3.png"
            }
          ]
        },
        {
          name: "Illustrations",
          value: "illustrations",
          description: "Popular cliparts",
          data: []
        },
        {
          name: "Shapes",
          value: "shapes",
          description: "Select shapes",
          data: [
            {
              value: "shapeSquare",
              imageURL: "/basicImages/elements/shapeSquare.png"
            },
            {
              value: "shapeCircle",
              imageURL: "/basicImages/elements/shapeCircle.png"
            },
            {
              value: "shapeTriangle",
              imageURL: "/basicImages/elements/shapeTriangle.png"
            },
            {
              value: "shapeRoundedSquare",
              imageURL: "/basicImages/elements/shapeRoundedSquare.png"
            },
            {
              value: "shapeStar",
              imageURL: "/basicImages/elements/shapeStar.png"
            },
            {
              value: "shapePentagon",
              imageURL: "/basicImages/elements/shapePentagon.png"
            },
            {
              value: "shapeLine",
              imageURL: "/basicImages/elements/shapeLine.png"
            },
            {
              value: "shapeArrow",
              imageURL: "/basicImages/elements/shapeArrow.png"
            }
          ]
        },
        {
          name: "Stock Photos",
          value: "stockPhotos",
          data: []
        },
        {
          name: "My Images",
          value: "myImages",
          data: []
        }
      ],
      elementsImages: []
    };
  }
  componentDidMount() {
    window.addEventListener("keypress", e => {
      if (e.keyCode === 13) {
        console.log(this.state);
      }
    });

    this.getIllustrationsFromPixabay();
    this.getStockFromPixabay();
  }
  getIllustrationsFromPixabay() {
    $.ajax({
      url:
        "https://pixabay.com/api/?key=9664297-69e14e2845948f27db0b182bb&q=(cutlery%20OR%20vegetables%20OR%20drinks%20OR%20meat%20OR%20fish)&image_type=illustration&category=food&order=popular&per_page=50&safesearch=true"
    }).done(dataPixabay => {
      let elementsData = this.state.elementsData;
      elementsData[1].data = dataPixabay.hits.map((object, index) => {
        const iconImage = {
          value: object.id,
          imageURL: object.largeImageURL
        };
        return iconImage;
      });
      this.setState({ elementsData });
    });
  }
  getStockFromPixabay() {
    $.ajax({
      url:
        "https://pixabay.com/api/?key=9664297-69e14e2845948f27db0b182bb&q=(cutlery%20OR%20vegetables%20OR%20drinks%20OR%20meat%20OR%20fish)&image_type=photo&category=food&order=popular&per_page=50&safesearch=true"
    }).done(dataPixabay => {
      let elementsData = this.state.elementsData;
      elementsData[3].data = dataPixabay.hits.map((object, index) => {
        const iconImage = {
          value: object.id,
          imageURL: object.largeImageURL
        };
        return iconImage;
      });
      this.setState({ elementsData });
    });
  }

  elementsTitleClicked = styleName => {
    // Open or Close toolbar section when click
    const openedMenu = this.state.openedMenu;
    const styleNamePos = openedMenu.indexOf(styleName);
    if (styleNamePos === -1) {
      openedMenu.push(styleName);
      if (styleName === "accents") {
        this.props.navigateSubSection(editorSubSections.ELEMENTS_ACCENT);
      } else if (styleName === "illustrations") {
        this.props.navigateSubSection(editorSubSections.ELEMENTS_ILLUSTRATION);
      } else if (styleName === "shapes") {
        this.props.navigateSubSection(editorSubSections.ELEMENTS_SHAPE);
      }
    } else {
      openedMenu.splice(styleNamePos, 1);
    }

    this.setState({ openedMenu });
  };

  addItem = (itemType, itemData) => {
    // Handle Adding new Elements to Menu
    /*    if (itemType === "accents") {
      this.props.addAccent(itemData);
    } else if (itemType === "illustrations") {
      this.props.addAccent()
    } */
    this.props.addElement(itemType, itemData, this.props.pageNumber - 1);
  };

  render() {
    return (
      <div
        className={
          "toolbar elementsToolbar " +
          (this.props.navigation.editorSection === editorSections.ELEMENT
            ? ""
            : "inActive")
        }
      >
        <p id="toolbarTitle">ELEMENTS</p>
        {this.state.elementsData.map((menuData, index) => (
          <div className="toolbarSection" key={index}>
            <div
              onClick={() => this.elementsTitleClicked(menuData.value)}
              className={
                "elementsTitle " +
                (this.state.openedMenu.indexOf(menuData.value) > -1
                  ? "active"
                  : "")
              }
            >
              <img
                className={
                  "expandIcon " +
                  (this.state.openedMenu.indexOf(menuData.value) > -1
                    ? "inActive"
                    : "")
                }
                src="/basicImages/smallArrow.png"
                alt="Expand Menu"
              />
              <img
                className={
                  "expandIcon " +
                  `${menuData.value} ` +
                  (this.state.openedMenu.indexOf(menuData.value) > -1
                    ? ""
                    : "inActive")
                }
                src="/basicImages/activeSmallArrow.png"
                alt="Expand Menu"
              />
              <p>{menuData.name}</p>
            </div>
            <div
              className={
                "elementsContent " +
                (this.state.openedMenu.indexOf(menuData.value) > -1
                  ? "expanded"
                  : "")
              }
            >
              <div>
                {menuData.value === "illustrations" && (
                  <div className="illustrationSearch">
                    <p>Search illustrations</p>
                    <Input icon placeholder="Search illustrations">
                      <input />
                      <Icon name="search" />
                    </Input>
                  </div>
                )}
                <p>{menuData.description}</p>
                <div className="menuOptions">
                  {menuData.data.map((menuOptions, index2) => {
                    return (
                      <div
                        onClick={() =>
                          this.addItem(menuData.value, menuOptions)
                        }
                        key={index2}
                      >
                        <img
                          className="sectionColumnsItem"
                          src={menuOptions.imageURL}
                          alt={menuOptions.value}
                        />
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    );
  }
}
