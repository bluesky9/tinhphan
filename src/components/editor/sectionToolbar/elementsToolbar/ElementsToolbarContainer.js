import { connect } from "react-redux";
import {
  navigateEditorSubSection,
  addElement
} from "../../../../redux/actions";
import ElementsToolbar from "./ElementsToolbar";
// import Redux Actions with the following line
// import {} from  ''

const mapStateToProps = (state, ownProps) => {
  return Object.assign(
    {},
    {
      navigation: state.navigation,
      pageNumber: state.editor.page
    }
  );
};

const mapDispatchToProps = dispatch => {
  return {
    navigateSubSection: subSection =>
      dispatch(navigateEditorSubSection(subSection)),
    addElement: (elementType, elementData, pageNumber) =>
      dispatch(addElement(elementType, elementData, pageNumber))
  };
};

const ElementsToolbarContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ElementsToolbar);

export default ElementsToolbarContainer;
