export const elementsData = [
  {
    name: "Accents",
    value: "accents",
    description: "Select accents",
    data: [
      {
        value: "accentBlank",
        imageURL: "/basicImages/elements/accentBlank.png"
      },
      {
        value: "accentTop",
        imageURL: "/basicImages/elements/accentTop.png"
      },
      {
        value: "accentHot",
        imageURL: "/basicImages/elements/accentHot.png"
      },
      {
        value: "accentNew",
        imageURL: "/basicImages/elements/accentNew.png"
      },
      {
        value: "accentSpec",
        imageURL: "/basicImages/elements/accentSpec.png"
      },
      {
        value: "accentMenu",
        imageURL: "/basicImages/elements/accentMenu.png"
      },
      {
        value: "accentVetageble",
        imageURL: "/basicImages/elements/accentVegetable.png"
      },
      {
        value: "accentNonVetageble",
        imageURL: "/basicImages/elements/accentNonVegetable.png"
      },
      {
        value: "accentSpicy1",
        imageURL: "/basicImages/elements/accentSpicy1.png"
      },
      {
        value: "accentSpicy2",
        imageURL: "/basicImages/elements/accentSpicy2.png"
      },
      {
        value: "accentSpicy3",
        imageURL: "/basicImages/elements/accentSpicy3.png"
      }
    ]
  },
  {
    name: "Illustrations",
    value: "illustrations",
    description: "Popular cliparts",
    data: [
      {
        value: "clipartBanana",
        imageURL: "/basicImages/elements/clipartBanana.png"
      },
      {
        value: "clipartPizza",
        imageURL: "/basicImages/elements/clipartPizza.png"
      },
      {
        value: "clipartChese",
        imageURL: "/basicImages/elements/clipartChese.png"
      },
      {
        value: "clipartMeat",
        imageURL: "/basicImages/elements/clipartMeat.png"
      },
      {
        value: "clipartEgg",
        imageURL: "/basicImages/elements/clipartEgg.png"
      },
      {
        value: "clipartCoconut",
        imageURL: "/basicImages/elements/clipartCoconut.png"
      },
      {
        value: "clipartPotato",
        imageURL: "/basicImages/elements/clipartPotato.png"
      },
      {
        value: "clipartFlower",
        imageURL: "/basicImages/elements/clipartFlower.png"
      },
      {
        value: "clipartFruit",
        imageURL: "/basicImages/elements/clipartFruit.png"
      },
      {
        value: "clipartApple",
        imageURL: "/basicImages/elements/clipartApple.png"
      },
      {
        value: "clipartSandwich",
        imageURL: "/basicImages/elements/clipartSandwich.png"
      },
      {
        value: "clipartGabbage",
        imageURL: "/basicImages/elements/clipartGabbage.png"
      },
      {
        value: "clipartPizza2",
        imageURL: "/basicImages/elements/clipartPizza2.png"
      },
      {
        value: "clipartStrawberry",
        imageURL: "/basicImages/elements/clipartStrawberry.png"
      },
      {
        value: "clipartSomething",
        imageURL: "/basicImages/elements/clipartSomething.png"
      },
      {
        value: "clipartLemon",
        imageURL: "/basicImages/elements/clipartLemon.png"
      }
    ]
  },
  {
    name: "Shapes",
    value: "shapes",
    description: "Select shapes",
    data: [
      {
        value: "shapeSquare",
        imageURL: "/basicImages/elements/shapeSquare.png"
      },
      {
        value: "shapeCircle",
        imageURL: "/basicImages/elements/shapeCircle.png"
      },
      {
        value: "shapeTriangle",
        imageURL: "/basicImages/elements/shapeTriangle.png"
      },
      {
        value: "shapeRoundedSquare",
        imageURL: "/basicImages/elements/shapeRoundedSquare.png"
      },
      {
        value: "shapeStar",
        imageURL: "/basicImages/elements/shapeStar.png"
      },
      {
        value: "shapePentagon",
        imageURL: "/basicImages/elements/shapePentagon.png"
      },
      {
        value: "shapeLine",
        imageURL: "/basicImages/elements/shapeLine.png"
      },
      {
        value: "shapeArrow",
        imageURL: "/basicImages/elements/shapeArrow.png"
      }
    ]
  },
  {
    name: "Stock Photos",
    value: "stockPhotos",
    data: []
  },
  {
    name: "My Images",
    value: "myImages",
    data: []
  }
];
