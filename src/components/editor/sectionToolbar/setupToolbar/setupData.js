export const setupData = [
  {
    name: "Page Column",
    value: "pageColumn",
    data: []
  },
  {
    name: "Page Margins",
    value: "pageMargins",
    data: []
  },
  {
    name: "Page Cover",
    value: "pageCover",
    data: []
  },
  {
    name: "Page Numbering",
    value: "pageNumbering",
    data: []
  },
  {
    name: "Table of Content",
    value: "tableContent",
    data: []
  },
  {
    name: "Wine String Order",
    value: "wineOrder",
    data: []
  },
  {
    name: "Grouping and Ordering",
    value: "groupingOrdering",
    data: []
  }
];
