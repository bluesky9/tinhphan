import React from "react";
import { Checkbox } from "semantic-ui-react";
import "../../../../../styles/editor/groupOrder.css";
import { groupOrderStyle } from "./GroupOrderData";

export default class GroupOrder extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openedMenu: [],
      styleItemValue: ""
    };
  }
  styleTitleClicked = styleName => {
    const openedMenu = this.state.openedMenu;
    const styleNamePos = openedMenu.indexOf(styleName);
    if (styleNamePos === -1) {
      openedMenu.push(styleName);
    } else {
      openedMenu.splice(styleNamePos, 1);
    }

    this.setState({ openedMenu });
  };

  styleItemSelected = groupStyle => {
    let styleItemValue = this.state.styleItemValue;
    styleItemValue = groupStyle;

    this.setState({ styleItemValue });
  };
  render() {
    return (
      <div className="groupOrder">
        {groupOrderStyle.map((groupStyle, index) => (
          <div
            className={
              "groupTitle " +
              (this.state.openedMenu.indexOf(groupStyle.value) > -1
                ? "active"
                : "")
            }
          >
            <div
              onClick={() => this.styleTitleClicked(groupStyle.value)}
              className={
                "groupContent " +
                (this.state.openedMenu.indexOf(groupStyle.value) > -1
                  ? "active"
                  : "")
              }
              style={{ position: "relative" }}
            >
              <img
                className="groupCheck"
                src="/basicImages/menuItemClick.png"
                alt="Selected Layout"
              />
              <img
                className={
                  "expandIcon " +
                  (this.state.openedMenu.indexOf(groupStyle.value) > -1
                    ? "inActive"
                    : "")
                }
                src="/basicImages/smallArrow.png"
                alt="Expand Menu"
              />
              <img
                className={
                  "expandIcon " +
                  `${groupStyle.value} ` +
                  (this.state.openedMenu.indexOf(groupStyle.value) > -1
                    ? ""
                    : "inActive")
                }
                src="/basicImages/activeSmallArrow.png"
                alt="Expand Menu"
              />
              <p>{groupStyle.name}</p>
            </div>
            <div
              className={
                "group2Content " +
                (this.state.openedMenu.indexOf(groupStyle.value) > -1
                  ? "expanded"
                  : "")
              }
            >
              {groupStyle.data.map((groupInfo, index2) => (
                <div key={index2} className="group2Section">
                  <div
                    className={
                      "styleTitle2 " +
                      (this.state.openedMenu.indexOf(groupInfo.value) > -1
                        ? "active"
                        : "")
                    }
                    onClick={() => this.styleTitleClicked(groupInfo.value)}
                    style={{ position: "relative" }}
                  >
                    <img
                      className={
                        "expandIcon " +
                        (this.state.openedMenu.indexOf(groupInfo.value) > -1
                          ? "inActive"
                          : "")
                      }
                      src="/basicImages/smallArrow.png"
                      alt="Expand Menu"
                    />
                    <img
                      className={
                        "expandIcon " +
                        `${groupInfo.value} ` +
                        (this.state.openedMenu.indexOf(groupInfo.value) > -1
                          ? ""
                          : "inActive")
                      }
                      src="/basicImages/activeSmallArrow.png"
                      alt="Expand Menu"
                    />
                    <p>{groupInfo.name}</p>
                  </div>
                  <div
                    className={
                      "group3Content " +
                      (this.state.openedMenu.indexOf(groupInfo.value) > -1
                        ? "expanded"
                        : "")
                    }
                  >
                    <div className="subgroupDesc">Select subgroup</div>
                    {groupInfo.data.map((groupInfo2, index3) => (
                      <div key={index3} className="group3Section">
                        <div
                          className={
                            "styleTitle3 " +
                            (this.state.openedMenu.indexOf(groupInfo2.value) >
                            -1
                              ? "active"
                              : "")
                          }
                        />
                        <div
                          onClick={() =>
                            this.styleTitleClicked(groupInfo2.value)
                          }
                          style={{ position: "relative" }}
                        >
                          <div className="Checkbox">
                            <Checkbox label={groupInfo2.label} />
                          </div>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              ))}
            </div>
          </div>
        ))}
      </div>
    );
  }
}
