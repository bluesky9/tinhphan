export const groupOrderStyle = [
  {
    name: "Country",
    value: "country",
    data: []
  },
  {
    name: "Body",
    value: "body",
    data: []
  },
  {
    name: "Type",
    value: "type",
    data: []
  },
  {
    name: "Serving",
    value: "serving",
    data: [
      {
        name: "Glass",
        value: "glass",
        data: []
      },
      {
        name: "1/2 Bottle",
        value: "halfBottle",
        data: []
      },
      {
        name: "Bottle",
        value: "bottle",
        data: [
          {
            label: "Countries",
            value: "countries"
          },
          {
            label: "Type",
            value: "type"
          },
          {
            label: "Regions",
            value: "regions"
          },
          {
            label: "Areas",
            value: "areas"
          },
          {
            label: "Body",
            value: "body2"
          },
          {
            label: "Serving",
            value: "serving"
          },
          {
            label: "Grapes",
            value: "grapes"
          },
          {
            label: "Winemaker",
            value: "winemaker"
          }
        ]
      }
    ]
  }
];
