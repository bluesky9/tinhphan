/* global $ */
import React from "react";
import "../../../../../styles/editor/wineOrder.css";
import {
  DraggableWineStyleDetail,
  DroppableBlankDropTarget
} from "./dragHelper";

const wineOrderStyle = [
  {
    value: "singleCol",
    imageURL: "/basicImages/setupToolbar/wineSingleCol.png",
    activeImageURL: "/basicImages/setupToolbar/wineSingleColActive.png"
  },
  {
    value: "twoCol",
    imageURL: "/basicImages/setupToolbar/wineTwoCol.png",
    activeImageURL: "/basicImages/setupToolbar/wineTwoColActive.png"
  },
  {
    value: "trippleCol",
    imageURL: "/basicImages/setupToolbar/wineTrippleCol.png",
    activeImageURL: "/basicImages/setupToolbar/wineTrippleColActive.png"
  }
];

const rawWineOrderDetails = [
  "Subtype",
  "Area",
  "Rating",
  "Name",
  "Region",
  "AddText",
  "semiColon"
];

export default class WineOrder extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      wineOrderStyle: {
        selectedStyle: "singleCol",
        styleDetails: [["Vintage"], ["Producer"], ["Country"], []],
        styleDetailsColumnWidth: [100, 0, 0]
      },
      dropTargetData: {
        targetValue: "",
        expandDirection: "",
        targetParent: -1
      },
      isUpdatingWidth: false,
      widthInputData: {
        changingInput: -1,
        mockData: ""
      }
    };
  }

  componentWillMount() {
    // Init Data for Class
    const { wineOrderStyle } = this.state;
    wineOrderStyle.styleDetails[3] = rawWineOrderDetails;
    this.setState({ wineOrderStyle });
  }

  componentDidMount() {
    window.addEventListener("keypress", e => {
      if (e.keyCode === 13 && this.state.widthInputData.changingInput !== -1) {
        this.finishWidthInputChange();
      }
    });
  }

  handleStyleSelected = selectedStyle => {
    // Save Order Style for Wine List
    const styleDetails = this.state.wineOrderStyle.styleDetails;
    let styleDetailsColumnWidth = this.state.wineOrderStyle
      .styleDetailsColumnWidth;

    if (selectedStyle !== "trippleCol") {
      styleDetails[3] = styleDetails[3].concat(styleDetails[2]);
      styleDetails[2] = [];
      styleDetailsColumnWidth = [50, 50, 0];
    }

    if (selectedStyle === "singleCol") {
      styleDetails[3] = styleDetails[3].concat(styleDetails[1]);
      styleDetails[1] = [];
      styleDetailsColumnWidth = [100, 0, 0];
    }

    if (selectedStyle === "trippleCol") {
      styleDetailsColumnWidth = [33.3, 33.3, 33.3];
    }
    const newWineOrderStyle = Object.assign({}, this.state.wineOrderStyle, {
      selectedStyle,
      styleDetails,
      styleDetailsColumnWidth
    });

    this.setState({ wineOrderStyle: newWineOrderStyle });
  };

  renderColumnDetailsContainer = (styleDetails, selectedStyle) => {
    // Render Columns Style Details Section
    return [0, 1, 2, -1].map((sectionNumber, index3) => {
      let containerClassName = "";
      let sectionTitle = "";
      switch (sectionNumber) {
        case 0:
          containerClassName = "firstColumnDetails";
          sectionTitle = "First Column";
          break;
        case 1:
          containerClassName = "secondColumnDetails";
          sectionTitle = "Second Column";
          break;
        case 2:
          containerClassName = "thirdColumnDetails";
          sectionTitle = "Third Column";
          break;
        default:
          containerClassName = "unselectedColumnDetails";
          sectionTitle = "Not selected";
      }

      if (sectionNumber === 1 && selectedStyle === "singleCol") {
        return;
      } else if (sectionNumber === 2 && selectedStyle !== "trippleCol") {
        return;
      }

      return (
        <div className={"columnDetailsContainer " + containerClassName}>
          <p
            id={sectionNumber === 0 ? "firstColumnTitle" : ""}
            className={
              sectionNumber === 0 && selectedStyle === "singleCol" && "hidden"
            }
          >
            {sectionTitle}
          </p>
          {styleDetails[index3].length > 0 ? (
            styleDetails[index3].map((styleDetail, index2) => (
              <DraggableWineStyleDetail
                setExpandedTarget={this.setExpandedTarget}
                parent={index3}
                key={index2}
                value={styleDetail}
                unexpandTarget={this.unexpandTarget}
                addItem={this.addItem}
                lastItem={index2 === styleDetails[index3].length - 1}
              />
            ))
          ) : (
            <DroppableBlankDropTarget
              addFirstItem={this.addFirstItem}
              clearDropTarget={this.clearDropTarget}
              sectionNumber={sectionNumber}
            />
          )}
        </div>
      );
    });
  };

  addFirstItem = (destinationSection, itemData) => {
    // Add The first Item to a blank section
    if (destinationSection === -1) {
      destinationSection = 3;
    }

    const { wineOrderStyle } = this.state;
    const currentItemPos = wineOrderStyle.styleDetails[itemData.parent].indexOf(
      itemData.value
    );
    wineOrderStyle.styleDetails[itemData.parent].splice(currentItemPos, 1);
    wineOrderStyle.styleDetails[destinationSection].push(itemData.value);

    this.setState({ wineOrderStyle });
  };

  addItem = itemData => {
    console.log(this.state.dropTargetData);

    // Handle Dropped Item and Add to Section
    if (this.state.dropTargetData.targetValue.length === 0) {
      return;
    }
    const { wineOrderStyle } = this.state;
    const currentItemPos = wineOrderStyle.styleDetails[itemData.parent].indexOf(
      itemData.value
    );
    wineOrderStyle.styleDetails[itemData.parent].splice(currentItemPos, 1);

    let addPos = wineOrderStyle.styleDetails[
      this.state.dropTargetData.targetParent
    ].indexOf(this.state.dropTargetData.targetValue);

    if (this.state.dropTargetData.expandDirection === "right") {
      addPos += 1;
    }

    wineOrderStyle.styleDetails[this.state.dropTargetData.targetParent].splice(
      addPos,
      0,
      itemData.value
    );

    this.setState({ wineOrderStyle });

    this.clearDropTarget();
  };

  clearDropTarget = () => {
    // Clear Drop Target when Drag to Blank Section
    this.unexpandTarget();
    const newDropTargetData = Object.assign({}, this.state.dropTargetData, {
      targetValue: "",
      expandDirection: ""
    });

    this.setState({ dropTargetData: newDropTargetData });
  };

  setExpandedTarget = (
    targetValue,
    expandDirection,
    targetParent,
    distance
  ) => {
    // Prepare Space for Dropping Item
    if (
      targetValue !== this.state.dropTargetData.targetValue ||
      expandDirection !== this.state.dropTargetData.expandDirection
    ) {
      $(".styleDetail.expanded")
        .removeClass("expanded")
        .css("margin", "0px 5px 5px 0px");
    }

    const newDropTargetData = Object.assign({}, this.state.dropTargetData, {
      targetValue,
      expandDirection,
      targetParent
    });

    this.setState({ dropTargetData: newDropTargetData });

    const cssAttribute =
      expandDirection === "left" ? "margin-left" : "margin-right";

    const target = $(`.styleDetail.${targetValue}`);
    if (!target.hasClass("expanded")) {
      target.addClass("expanded");
      target.css(cssAttribute, `+=${Math.floor(distance)}px`);
    }
  };

  unexpandTarget() {
    // Finish Drag and Drop
    console.log("run run");

    $(".styleDetail")
      .removeClass("expanded")
      .css({
        margin: "0px",
        marginRight: "5px",
        marginBottom: "5px"
      });
  }

  holdAdjustColumnWidth = (columnNumber, isIncrement) => {
    // Handle user holds Increase or Decrease width buttons
    this.setState({ isUpdatingWidth: true });
    this.adjustWidthInterval = setInterval(
      () => this.adjustColumnWidth(columnNumber, isIncrement),
      100
    );
  };

  releaseAdjustColumnWidth = () => {
    this.setState({ isUpdatingWidth: false });
    clearInterval(this.adjustWidthInterval);
  };

  adjustColumnWidth = (columnNumber, isIncrement) => {
    const styleDetailsColumnWidth = this.state.wineOrderStyle
      .styleDetailsColumnWidth;
    if (isIncrement) {
      styleDetailsColumnWidth[columnNumber]++;
    } else {
      styleDetailsColumnWidth[columnNumber]--;
    }

    const newWineOrderStyle = Object.assign({}, this.state.wineOrderStyle, {
      styleDetailsColumnWidth
    });

    this.setState({ wineOrderStyle: newWineOrderStyle });
  };

  widthInputChange = (e, columnNumber) => {
    let widthInputData = this.state.widthInputData;
    if (widthInputData.changingInput === -1) {
      widthInputData.changingInput = columnNumber;
    }

    widthInputData.mockData = e.target.value;

    widthInputData = Object.assign(
      {},
      this.state.widthInputData,
      widthInputData
    );

    this.setState({ widthInputData });
  };

  finishWidthInputChange = () => {
    let { styleDetailsColumnWidth } = this.state.wineOrderStyle;

    let { widthInputData } = this.state;

    if (isNaN(Number(widthInputData.mockData))) {
      const number = Number(widthInputData.mockData.split("%")[0]);
      if (isNaN(number)) {
        styleDetailsColumnWidth[widthInputData.changingInput] = 50;
      } else {
        styleDetailsColumnWidth[widthInputData.changingInput] = number;
      }
    } else {
      styleDetailsColumnWidth[widthInputData.changingInput] = Number(
        widthInputData.mockData.split("%")[0]
      );
    }

    widthInputData.changingInput = -1;
    widthInputData.mockData = "";

    const wineOrderStyle = Object.assign({}, this.state.wineOrderStyle, {
      styleDetailsColumnWidth
    });

    this.setState({ wineOrderStyle, widthInputData });
  };

  render() {
    const {
      selectedStyle,
      styleDetails,
      styleDetailsColumnWidth
    } = this.state.wineOrderStyle;

    return (
      <div className="wineOrder">
        <div id="wineOrderStyle">
          <p>Select Column Style</p>
          {wineOrderStyle.map((styleData, index) => (
            <div
              key={index}
              className="styleOption"
              onClick={() => this.handleStyleSelected(styleData.value)}
            >
              <img
                src={styleData.imageURL}
                className={selectedStyle !== styleData.value && "active"}
                alt={styleData.value}
              />
              <img
                src={styleData.activeImageURL}
                className={
                  selectedStyle === styleData.value && "active selected"
                }
                alt={styleData.value}
              />
            </div>
          ))}
        </div>
        <div id="wineOrderDetails">
          <p>Order item by drag and drop</p>
          {this.renderColumnDetailsContainer(styleDetails, selectedStyle)}
          <div id="columnSettingsContainer">
            <p>Column Settings</p>
            {[0, 1, 2].map((number, index) => (
              <ColumnSettings
                releaseAdjustColumnWidth={this.releaseAdjustColumnWidth}
                adjustColumnWidth={this.adjustColumnWidth}
                value={styleDetailsColumnWidth[number]}
                selectedStyle={selectedStyle}
                index={index}
                holdAdjustColumnWidth={this.holdAdjustColumnWidth}
                key={index}
                finishWidthInputChange={this.finishWidthInputChange}
                widthInputChange={this.widthInputChange}
                widthInputData={this.state.widthInputData}
              />
            ))}
          </div>
        </div>
      </div>
    );
  }
}

const ColumnSettings = props => {
  let sectionClass = "",
    sectionTitle = "";
  switch (props.index) {
    case 0:
      sectionClass = "firstColumnSettings";
      sectionTitle = "First Column";
      break;
    case 1:
      (sectionClass = "secondColumnSettings"), (sectionTitle = "Second Column");
      break;
    default:
      (sectionClass = "thirdColumnSettings"), (sectionTitle = "Third Column");
  }

  if (props.index === 2 && props.selectedStyle !== "trippleCol") {
    return <div />;
  }
  if (props.index === 1 && props.selectedStyle === "singleCol") {
    return <div />;
  }
  return (
    <div className={"columnSettings " + sectionClass}>
      <p>{sectionTitle}</p>
      <div
        onMouseDown={() => props.holdAdjustColumnWidth(props.index, false)}
        onMouseLeave={() => props.releaseAdjustColumnWidth()}
        onMouseUp={() => props.releaseAdjustColumnWidth()}
        onClick={() => props.adjustColumnWidth(props.index, false)}
        className="minusSign columnSettingButton"
      >
        <div>
          <img src="/basicImages/setupToolbar/minusSign.png" alt="Decrease" />
        </div>
      </div>
      <input
        onChange={e => props.widthInputChange(e, props.index)}
        onBlur={() => props.finishWidthInputChange(props.index)}
        value={
          props.widthInputData.changingInput === props.index
            ? props.widthInputData.mockData
            : props.value + "%"
        }
        type="text"
      />
      <div
        onMouseDown={() => props.holdAdjustColumnWidth(props.index, true)}
        onMouseLeave={() => props.releaseAdjustColumnWidth()}
        onMouseUp={() => props.releaseAdjustColumnWidth()}
        onClick={() => props.adjustColumnWidth(props.index, true)}
        className="addSign columnSettingButton"
      >
        <div>
          <img src="/basicImages/setupToolbar/addSign.png" alt="Increase" />
        </div>
      </div>
    </div>
  );
};
