/* global $ */
import React from "react";
import { DragSource, DropTarget } from "react-dnd";
import { dragTypes } from "./dragTypes";
import { flow } from "lodash";

/***
 *
 * Start Style Item Drag Handling
 *
 ***/

const styleDetailSource = {
  beginDrag(props) {
    const thisItemWidth = $(`.styleDetail.${props.value}`).innerWidth();
    return { value: props.value, parent: props.parent, thisItemWidth };
  },
  endDrag(props, monitor) {
    props.unexpandTarget();
    props.addItem(monitor.getItem());
  }
};

const styleDetailDragCollect = (connect, monitor) => {
  return {
    connectDragSoure: connect.dragSource(),
    isDragging: monitor.isDragging()
  };
};

class WineStyleDetail extends React.Component {
  render() {
    const { connectDragSoure, isDragging } = this.props;
    return connectDragSoure(
      <div
        className={
          "styleDetail " + this.props.value + (isDragging ? " dragging" : "")
        }
      >
        {this.props.value === "semiColon" ? ";" : this.props.value}
        <FinalStyleDetailDropPad
          value={this.props.value}
          parent={this.props.parent}
          targetType="left"
          setExpandedTarget={this.props.setExpandedTarget}
          unexpandTarget={this.props.unexpandTarget}
          addItem={this.props.addItem}
        />
        <FinalStyleDetailDropPad
          value={this.props.value}
          parent={this.props.parent}
          targetType="right"
          setExpandedTarget={this.props.setExpandedTarget}
          unexpandTarget={this.props.unexpandTarget}
          addItem={this.props.addItem}
        />
        {this.props.lastItem && (
          <FinalStyleDetailDropPad
            value={this.props.value}
            parent={this.props.parent}
            targetType="right"
            lastItem={true}
            setExpandedTarget={this.props.setExpandedTarget}
            unexpandTarget={this.props.unexpandTarget}
            addItem={this.props.addItem}
          />
        )}
      </div>
    );
  }
}

export const DraggableWineStyleDetail = DragSource(
  dragTypes.styleDetail,
  styleDetailSource,
  styleDetailDragCollect
)(WineStyleDetail);

/***
 *
 * End Style Item Drag Handling
 * Start Style Item Drop Target Handling
 *
 ***/

const styleDetailDropPadTarget = {
  drop(props) {
    props.unexpandTarget();
  },

  canDrop(props, monitor) {
    const draggedItemValue = monitor.getItem();
    return !(props.parent === draggedItemValue.parent && props.parent === 3);
  },

  hover(props, monitor) {
    console.log("Hover");

    const draggedItemValue = monitor.getItem();
    if (props.parent === draggedItemValue.parent && props.parent === -1) {
      return;
    } else {
      props.setExpandedTarget(
        props.value,
        props.targetType,
        props.parent,
        monitor.getItem().thisItemWidth
      );
    }
  }
};

const styleDetailDropPadCollect = (connect, monitor) => {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop(),
    draggedItem: monitor.getItem()
  };
};

class StyleDetailDropPad extends React.Component {
  componentWillReceiveProps(nextProps) {
    if (this.props.isOver === false && nextProps.isOver === true) {
      const draggedItemValue = this.props.draggedItem;
      if (
        this.props.parent === draggedItemValue.parent &&
        draggedItemValue.parent === 3
      ) {
        return;
      } else {
        //this.props.setExpandedTarget(this.props.value, this.props.targetType);
      }
    }
  }
  render() {
    const {
      connectDropTarget,
      isOver,
      canDrop,
      targetType,
      lastItem
    } = this.props;

    return connectDropTarget(
      <div
        className={
          "dropTarget " +
          `${lastItem ? "last" : targetType} ${isOver} ${canDrop}`
        }
      />
    );
  }
}

const FinalStyleDetailDropPad = DropTarget(
  dragTypes.styleDetail,
  styleDetailDropPadTarget,
  styleDetailDropPadCollect
)(StyleDetailDropPad);

/***
 *
 * End Style Item Drop Target Handling
 * Start Blank Drop Target Handling
 *
 ***/

const blankDropTarget = {
  drop(props, monitor) {
    props.addFirstItem(props.sectionNumber, monitor.getItem());
  },
  hover(props) {
    props.clearDropTarget();
  }
};

const blankDropTargetCollect = (connect, momitor) => ({
  connectDropTarget: connect.dropTarget()
});

const BlankDropTargetPad = ({ connectDropTarget }) => {
  return connectDropTarget(<div className={"blankDropTarget"} />);
};

export const DroppableBlankDropTarget = DropTarget(
  dragTypes.styleDetail,
  blankDropTarget,
  blankDropTargetCollect
)(BlankDropTargetPad);
