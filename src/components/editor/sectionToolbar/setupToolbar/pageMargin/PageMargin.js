import React from "react";
import "../../../../../styles/editor/pageMargin.css";

export default class PageMargin extends React.Component {
  constructor(props) {
    super(props);
  }
  // IncrementItem = () => {
  //   this.setState({ value: this.props.marginData.left + 1 });
  // };
  // DecreaseItem = () => {
  //   this.setState({ value: this.props.marginData.left - 1 });
  // };
  render() {
    return (
      <div className="pageMargin">
        <div className="leftMargin">
          <img
            className="marginColumnsLeft"
            src="/basicImages/marginColumnsLeft.png"
            alt="leftMargin"
          />
          <div
            onClick={() => this.props.DecreaseItem("left")}
            className="minusSign columnSettingButton"
          >
            <div>
              <img
                src="/basicImages/setupToolbar/minusSign.png"
                alt="Decrease"
              />
            </div>
          </div>
          <input
            type="number"
            className="input"
            value={this.props.marginData.left}
            onKeyPress={this.props.finishMarginInputChange}
            onChange={e => this.props.handleMarginChanged(e, "left")}
          />
          <div
            onClick={() => this.props.IncrementItem("left")}
            className="addSign columnSettingButton"
          >
            <div>
              <img src="/basicImages/setupToolbar/addSign.png" alt="Increase" />
            </div>
          </div>
        </div>
        <div className="rightMargin">
          <img
            className="marginColumnsRight"
            src="/basicImages/marginColumnsRight.png"
            alt="rightMargin"
          />
          <div
            onClick={() => this.props.DecreaseItem("right")}
            className="minusSign columnSettingButton"
          >
            <div>
              <img
                src="/basicImages/setupToolbar/minusSign.png"
                alt="Decrease"
              />
            </div>
          </div>
          <input
            className="input"
            value={this.props.marginData.right}
            onChange={e => this.props.handleMarginChanged(e, "right")}
            type="number"
          />
          <div
            onClick={() => this.props.IncrementItem("right")}
            className="addSign columnSettingButton"
          >
            <div>
              <img src="/basicImages/setupToolbar/addSign.png" alt="Increase" />
            </div>
          </div>
        </div>
        <div className="centerMargin">
          <img
            className="marginColumnsCenter"
            src="/basicImages/marginColumnsCenter.png"
            alt="centerMargin"
          />
          <div
            onClick={() => this.props.DecreaseItem("middle")}
            className="minusSign columnSettingButton"
          >
            <div>
              <img
                src="/basicImages/setupToolbar/minusSign.png"
                alt="Decrease"
              />
            </div>
          </div>
          <input
            className="input"
            value={this.props.marginData.middle}
            onChange={e => this.props.handleMarginChanged(e, "middle")}
            type="number"
          />
          <div
            onClick={() => this.props.IncrementItem("middle")}
            className="addSign columnSettingButton"
          >
            <div>
              <img src="/basicImages/setupToolbar/addSign.png" alt="Increase" />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
