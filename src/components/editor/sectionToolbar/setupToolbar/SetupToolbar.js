import React from "react";
import { editorSections } from "../../../../assets/navigationKeywords";
import { Checkbox, Form } from "semantic-ui-react";
import "../../../../styles/editor/setupToolbar.css";
import { setupData } from "./setupData";
import GroupOrder from "./groupOrder/GroupOrder";
import PageMargin from "./pageMargin/PageMargin";
import WineOrder from "./wineOrder/WineOrder";

export default class SetupToolbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openedMenu: [],
      styleItemValue: "",
      setupData: {
        pageColumns: "",
        pageMargins: {
          left: 50,
          right: 50,
          middle: 50
        },
        pageNumbering: {
          startAt: "100",
          position: ""
        },
        tableOfContent: "",
        pageCover: ""
      }
    };
  }
  IncrementItem = marginType => {
    const { setupData } = this.state;
    // Increment page margin by 1
    // Boi vi co toi 3 cai margin, cho nen minh can phai co cai gi do de biet d
    // vay thi khi goi cai method nay, minh phai pass them mot tham so
    // vo de xac dinh margin nao can duoc increment
    // dung roi dung switch case cung duoc. nhung ma phai co cai gi de switch chu =]]
    // cho nen la pass cai marginType vo, roi switch no
    switch (marginType) {
      case "left":
        setupData.pageMargins.left += 1;
        break;
      case "right":
        setupData.pageMargins.right += 1;
        break;
      case "middle":
        setupData.pageMargins.middle += 1;
        break;
      default:
        break;
    }
    this.setState({ setupData });
  };
  DecreaseItem = marginType => {
    const { setupData } = this.state;
    switch (marginType) {
      case "left":
        setupData.pageMargins.left -= 1;
        break;
      case "right":
        setupData.pageMargins.right -= 1;
        break;
      case "middle":
        setupData.pageMargins.middle -= 1;
        break;
      default:
        break;
    }
    this.setState({ setupData });
  };
  // finishMarginInputChange = (e, marginType) => {
  //   const { setupData } = this.state;
  //   setupData.pageMargins.left = Number(e.target.value);
  //   if (isNaN(Number(e.target.value))) {
  //     const number = Number(e.target.value.split("%")[0]);
  //     if (isNaN(number)) {
  //       setupData.pageMargins.left = [50];
  //     } else {
  //       setupData.pageMargins.left = number;
  //     }
  //   } else {
  //     setupData.pageMargins.left = Number(e.target.value.split("%")[0]);
  //   }
  // };

  handleMarginChanged = (e, marginType) => {
    const { setupData } = this.state;
    switch (marginType) {
      case "left":
        setupData.pageMargins.left = Number(e.target.value);
        break;
      case "right":
        setupData.pageMargins.right = Number(e.target.value);
        break;
      case "middle":
        setupData.pageMargins.middle = Number(e.target.value);
        break;
      default:
        break;
    }

    this.setState({ setupData });
  };

  componentDidMount() {
    window.addEventListener("keypress", e => {
      if (e.keyCode === 13) {
        console.log(this.state);
      }
    });
  }

  styleTitleClicked = styleName => {
    const openedMenu = this.state.openedMenu;
    const styleNamePos = openedMenu.indexOf(styleName);
    if (styleNamePos === -1) {
      openedMenu.push(styleName);
    } else {
      openedMenu.splice(styleNamePos, 1);
    }

    this.setState({ openedMenu });
  };

  styleItemSelected = (setupStyle, setupInfo) => {
    let styleItemValue = this.state.styleItemValue;
    styleItemValue = setupStyle + "    " + setupInfo;

    this.setState({ styleItemValue });
  };
  handleChange = e => {
    console.log(e.target.value);
    const setupData = this.state.setupData;
    setupData.pageNumbering.startAt = Number(e.target.value);
    this.setState({ setupData });
  };

  onChange = (type, value) => {
    console.log(value);
    const setupData = this.state.setupData;
    switch (type) {
      case "column":
        setupData.pageColumns = value;
        this.setState({ setupData });
        break;
      case "cover":
        setupData.pageCover = value;
        this.setState({ setupData });
        break;
      default:
        null;
    }
  };
  handlePageNumbering = (e, data) => {
    console.log(data.value);
    const setupData = this.state.setupData;
    setupData.pageNumbering.position = data.value;
    this.setState({ setupData });
  };
  handleTableContent = (e, data) => {
    console.log(data.value);
    const setupData = this.state.setupData;
    setupData.tableOfContent = data.value;
    this.setState({ setupData });
  };
  render() {
    return (
      <div
        className={
          "toolbar setupToolbar " +
          (this.props.navigation.editorSection === editorSections.SETUP
            ? ""
            : "inActive")
        }
      >
        <p id="toolbarTitle">SETUP</p>
        {setupData.map((setupStyle, index) => (
          <div key={index} className="setupStyle">
            <div className="styleTitles">
              <div
                onClick={() => this.styleTitleClicked(setupStyle.value)}
                className="sectionTitle"
              >
                <img
                  className={
                    "expandIcon " +
                    (this.state.openedMenu.indexOf(setupStyle.value) > -1
                      ? "inActive"
                      : "")
                  }
                  src="/basicImages/smallArrow.png"
                  alt="Expand Menu"
                />
                <img
                  className={
                    "expandIcon " +
                    `${setupStyle.value} ` +
                    (this.state.openedMenu.indexOf(setupStyle.value) > -1
                      ? ""
                      : "inActive")
                  }
                  src="/basicImages/activeSmallArrow.png"
                  alt="Expand Menu"
                />
                <p>{setupStyle.name}</p>
              </div>

              <div
                className={
                  "setupSection " +
                  (this.state.openedMenu.indexOf(setupStyle.value) > -1
                    ? "expanded"
                    : "")
                }
              >
                {setupStyle.value === "pageColumn" ? (
                  <PageColumn onChange={this.onChange} />
                ) : setupStyle.value === "pageMargins" ? (
                  <PageMargin
                    finishMarginInputChange={this.finishMarginInputChange}
                    IncrementItem={this.IncrementItem}
                    DecreaseItem={this.DecreaseItem}
                    handleMarginChanged={this.handleMarginChanged}
                    marginData={this.state.setupData.pageMargins}
                  />
                ) : setupStyle.value === "pageCover" ? (
                  <PageCover onChange={this.onChange} />
                ) : setupStyle.value === "pageNumbering" ? (
                  <PageNumbering
                    startAt={this.state.setupData.pageNumbering.startAt}
                    position={this.state.setupData.pageNumbering.position}
                    onChange={this.handleChange}
                    handleCheckbox={this.handlePageNumbering}
                  />
                ) : setupStyle.value === "tableContent" ? (
                  <TableContent
                    tableOfContent={this.state.setupData.tableOfContent}
                    handleCheckbox={this.handleTableContent}
                  />
                ) : setupStyle.value === "wineOrder" ? (
                  <WineOrder />
                ) : setupStyle.value === "groupingOrdering" ? (
                  <GroupOrder />
                ) : null}
              </div>
            </div>
          </div>
        ))}
      </div>
    );
  }
}

const PageColumn = props => {
  return (
    <div>
      <div className="sectionContent">
        <div id="numberDesc2">Apply to current page</div>
        <div className="columnOptions">
          <img
            className="sectionColumnsItem"
            src="/basicImages/formatColumn1.png"
            alt="oneColumn"
            name="pageComlumns"
            onClick={() => props.onChange("column", "oneColumn")}
          />
          <img
            className="sectionColumnsItem"
            src="/basicImages/formatColumn2.png"
            alt="twoColumns"
            name="pageComlumns"
            onClick={() => props.onChange("column", "twoColumns")}
          />
          <img
            className="sectionColumnsItem"
            src="/basicImages/formatColumn3.png"
            alt="threeColumns"
            name="pageComlumns"
            onClick={() => props.onChange("column", "threeColumns")}
          />
          <img
            className="sectionColumnsItem"
            src="/basicImages/formatColumn4.png"
            alt="twoSmallLeft"
            name="pageComlumns"
            onClick={() => props.onChange("column", "twoSmallLeft")}
          />
          <img
            className="sectionColumnsItem"
            src="/basicImages/formatColumn5.png"
            alt="twoSmallRight"
            name="pageComlumns"
            onClick={() => props.onChange("column", "twoSmallRight")}
          />
        </div>
      </div>
    </div>
  );
};
const PageCover = props => {
  return (
    <div>
      <div className="sectionContent">
        <button
          id="addFront"
          onClick={() => props.onChange("cover", "addFront")}
        >
          Add Front Cover
        </button>
        <button id="addBack" onClick={() => props.onChange("cover", "addBack")}>
          Add Back Cover
        </button>
      </div>
    </div>
  );
};
const PageNumbering = props => {
  return (
    <div className="sectionContent">
      <div className="number">
        <p id="numberDesc">Start Numbering from page</p>
        <input
          value={props.startAt}
          id="numberCount"
          type="number"
          name="startAt"
          onChange={props.onChange}
        />
        <p id="numberDesc2">Select a page number position</p>
      </div>
      <Form.Field>
        <Checkbox
          checked={props.position === "none"}
          onClick={props.handleCheckbox}
          className="checkbox"
          name="position"
          value="none"
          label="None"
        />
        <Checkbox
          checked={props.posotion === "bottomLeft"}
          onClick={props.handleCheckbox}
          className="checkbox"
          name="position"
          value="bottomLeft"
          label="Bottom Left"
        />
        <Checkbox
          checked={props.position === "bottomRight"}
          onClick={props.handleCheckbox}
          className="checkbox"
          name="position"
          value="bottomRight"
          label="Bottom Right"
        />
        <Checkbox
          checked={props.position === "bottomCenter"}
          onClick={props.handleCheckbox}
          className="checkbox"
          name="position"
          value="bottomCenter"
          label="Bottom Center"
        />
        <Checkbox
          checked={props.position === "topLeft"}
          onClick={props.handleCheckbox}
          className="checkbox"
          name="position"
          value="topLeft"
          label="Top Left"
        />
        <Checkbox
          checked={props.position === "topCenter"}
          onClick={props.handleCheckbox}
          className="checkbox"
          name="position"
          value="topCenter"
          label="Top Center"
        />
        <Checkbox
          checked={props.position === "topRight"}
          onClick={props.handleCheckbox}
          className="checkbox"
          name="position"
          value="topRight"
          label="Top Right"
        />
      </Form.Field>
    </div>
  );
};
const TableContent = props => {
  return (
    <div>
      <div className="sectionContent">
        <Checkbox
          checked={props.tableOfContent === "menuHeaders"}
          className="checkbox"
          onClick={props.handleCheckbox}
          name="tableOfContent"
          value="menuHeaders"
          label="Menu Item Headers"
        />
        <Checkbox
          className="checkbox"
          checked={props.tableOfContent === "menuSections"}
          onClick={props.handleCheckbox}
          name="position"
          value="menuSections"
          label="Menu Item Sections"
        />
      </div>
    </div>
  );
};
