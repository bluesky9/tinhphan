import { connect } from "react-redux";
import SetupToolbar from "./SetupToolbar";
// import Redux Actions with the following line
// import {} from  ''

const mapStateToProps = (state, ownProps) => {
  return Object.assign(
    {},
    {
      navigation: state.navigation
    }
  );
};

const mapDispatchToProps = dispatch => {
  return {
    // Dispatch here
  };
};

const SetupToolbarContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SetupToolbar);

export default SetupToolbarContainer;
