import { connect } from "react-redux";
import BackgroundToolbar from "./BackgroundToolbar";
// import Redux Actions with the following line
// import {} from  ''
import {
  toolbarActions
} from "../../../../redux/actions";

const mapStateToProps = (state, ownProps) => {
  return Object.assign(
    {},
    {
      navigation: state.navigation
    }
  );
};

const mapDispatchToProps = {
  setMenuBackground: toolbarActions.setMenuBackground,
  setBackGroundOptions: toolbarActions.setBackGroundOptions,
  clearBackgroundOptions: toolbarActions.clearBackgroundOptions
}

const BackgroundToolbarContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(BackgroundToolbar);

export default BackgroundToolbarContainer;
