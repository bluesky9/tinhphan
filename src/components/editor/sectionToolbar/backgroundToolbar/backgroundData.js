export const backgroundData = [
  {
    name: "Color",
    value: "color",
    description: "Color palette",
    data: [
      "607D8B",
      "239688",
      "E91E63",
      "673AB8",
      "8BC34A",
      "F44336",
      "2CBCD3",
      "2196F3"
    ]
  },
  {
    name: "Patterns",
    value: "patterns",
    description: "Select patterns",
    data: []
  },
  {
    name: "Texttures",
    value: "texttures",
    description: "Select textture",
    data: []
  },
  {
    name: "Stock photos",
    value: "stockPhotos",
    description: "Popular photos",
    data: []
  },
  {
    name: "My Images",
    value: "myImages",
    description: "Select image from your computer",
    data: []
  }
];
