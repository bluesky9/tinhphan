/* global $ */
import React from "react";
import { editorSections } from "../../../../assets/navigationKeywords";
import "../../../../styles/editor/backgroundToolbar.css";
// import { backgroundData } from "./backgroundData";
import { Input, Icon } from "semantic-ui-react";
import { ChromePicker } from "react-color";

export default class BackgroundToolbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openedMenu: [],
      selectedOptions: {
        color: "",
        texttures: "",
        stockPhotos: ""
      },
      backgroundData: [
        {
          name: "Color",
          value: "color",
          description: "Color palette",
          data: [
            "607D8B",
            "239688",
            "E91E63",
            "673AB8",
            "8BC34A",
            "F44336",
            "2CBCD3",
            "2196F3"
          ]
        },
        {
          name: "Patterns",
          value: "patterns",
          description: "Select patterns",
          data: []
        },
        {
          name: "Texttures",
          value: "texttures",
          description: "Select textture",
          data: []
        },
        {
          name: "Stock photos",
          value: "stockPhotos",
          description: "Popular photos",
          data: []
        },
        {
          name: "My Images",
          value: "myImages",
          description: "Select image from your computer",
          data: []
        }
      ],
      colorOptions: [],
      isColorPickerOpened: false,
      pickerColor: ""
    };
  }

  componentWillMount() {
    const colorOptions = this.state.backgroundData.find(
      a => a.value === "color"
    ).data;

    this.setState({ colorOptions: colorOptions });
  }

  // componentDidMount() {
  //   window.addEventListener("keypress", this.saveSelectedColor);
  // }

  componentDidMount() {
    window.addEventListener("keypress", e => {
      if (e.keyCode === 13) {
        console.log(this.state);
      } else this.saveSelectedColor;
    });
    this.getPatternsFromPixabay();
    this.getTextturesFromPixabay();
    this.getStockFromPixabay();
  }
  getPatternsFromPixabay() {
    $.ajax({
      url:
        "https://pixabay.com/api/?key=9664297-69e14e2845948f27db0b182bb&q=(stripe+OR+food+OR+check+OR+fish)+pattern&category=background&per_page=50&safesearch=true&image_type=illustration"
    }).done(dataPixabay => {
      let backgroundData = this.state.backgroundData;
      backgroundData[1].data = dataPixabay.hits.map((object, index) => {
        const iconImage = {
          value: object.id,
          imageURL: object.largeImageURL
        };
        return iconImage;
      });
      this.setState({ backgroundData });
    });
  }
  getTextturesFromPixabay() {
    $.ajax({
      url:
        "https://pixabay.com/api/?key=9664297-69e14e2845948f27db0b182bb&q=texture&category=background&per_page=50&safesearch=true"
    }).done(dataPixabay => {
      let backgroundData = this.state.backgroundData;
      backgroundData[2].data = dataPixabay.hits.map((object, index) => {
        const iconImage = {
          value: object.id,
          imageURL: object.largeImageURL
        };
        return iconImage;
      });
      this.setState({ backgroundData });
    });
  }
  getStockFromPixabay() {
    $.ajax({
      url:
        "https://pixabay.com/api/?key=9664297-69e14e2845948f27db0b182bb&q=(food%20OR%20restaurant%20OR%20drinks%20OR%20wine)&hp=&image_type=photo&order=popular&category=backgrounds&per_page=50&safesearch=true"
    }).done(dataPixabay => {
      let backgroundData = this.state.backgroundData;
      backgroundData[3].data = dataPixabay.hits.map((object, index) => {
        const iconImage = {
          value: object.id,
          imageURL: object.largeImageURL
        };
        return iconImage;
      });
      this.setState({ backgroundData });
    });
  }

  backgroundTitleClicked = styleName => {
    // Open or Close toolbar section when click
    const openedMenu = this.state.openedMenu;
    const styleNamePos = openedMenu.indexOf(styleName);
    if (styleNamePos === -1) {
      openedMenu.push(styleName);
    } else {
      openedMenu.splice(styleNamePos, 1);
    }

    if (styleName === "color" && this.state.isColorPickerOpened === true) {
      this.openColorPicker();
    }

    this.setState({ openedMenu });
  };

  handleItemBackgroundItemClick = (backgroundSection, itemValue) => {
    // Record Background Options selected by users
    const newSelection = {};
    newSelection[backgroundSection] = itemValue;

    const newSelectedOptions = Object.assign(
      {},
      this.state.selectedOptions,
      newSelection
    );
    
    this.setState({ selectedOptions: newSelectedOptions });

    const optionsData = {
      optionName: backgroundSection,
      data: this.state.backgroundData.find(item => item.value == backgroundSection).data.find(item => item.value == itemValue).imageURL
    }

    this.props.setBackGroundOptions(optionsData)
  };

  startUploadImage = () => {
    // Handle Upload Images
    $("#imageUpload > input").click();
  };

  handlePickerChange = color => {
    // Handle Color Picker Change
    const newColor = color.hex.split("#")[1];
    this.setState({ pickerColor: newColor });
    // set menu background
    this.props.setMenuBackground(color.hex);
  };

  onSelectColorOption = color => () => {
    // set menu background
    this.props.setMenuBackground("#" + color);
  }

  saveSelectedColor = e => {
    // Save user's selected color when Enter

    if (this.state.isColorPickerOpened === false || e.keyCode !== 13) return;
    const colorOptions = this.state.colorOptions;
    colorOptions.push(this.state.pickerColor);

    this.setState({ colorOptions });
    this.openColorPicker();
  };

  openColorPicker = () => {
    // Toggle Color Picker
    const isColorPickerOpened = this.state.isColorPickerOpened;

    this.setState({ isColorPickerOpened: !isColorPickerOpened });

    if (isColorPickerOpened === false) {
      $($(".backgroundContent")[0]).css("overflow", "visible");
    } else {
      $($(".backgroundContent")[0]).css("overflow", "hidden");
    }
  };

  render() {
    return (
      <div
        className={
          "toolbar backgroundToolbar " +
          (this.props.navigation.editorSection === editorSections.BACKGROUND
            ? ""
            : "inActive")
        }
      >
        <div id="mainBackgroundSearch">
          <Input icon placeholder="Search photo">
            <input />
            <Icon name="search" />
          </Input>
        </div>
        <p id="toolbarTitle">
          BACKGROUND <button onClick={this.props.clearBackgroundOptions}>Clear background</button>
        </p>
        {this.state.backgroundData.map((menuData, index) => (
          <div className="toolbarSection" key={index}>
            <div
              onClick={() => this.backgroundTitleClicked(menuData.value)}
              className={
                "backgroundTitle " +
                (this.state.openedMenu.indexOf(menuData.value) > -1
                  ? "active"
                  : "")
              }
            >
              <img
                className={
                  "expandIcon " +
                  (this.state.openedMenu.indexOf(menuData.value) > -1
                    ? "inActive"
                    : "")
                }
                src="/basicImages/smallArrow.png"
                alt="Expand Menu"
              />
              <img
                className={
                  "expandIcon " +
                  `${menuData.value} ` +
                  (this.state.openedMenu.indexOf(menuData.value) > -1
                    ? ""
                    : "inActive")
                }
                src="/basicImages/activeSmallArrow.png"
                alt="Expand Menu"
              />
              <p>{menuData.name}</p>
            </div>
            <div
              className={
                "backgroundContent " +
                (this.state.openedMenu.indexOf(menuData.value) > -1
                  ? "expanded"
                  : "")
              }
            >
              <div>
                {menuData.value === "stockPhotos" && (
                  <div className="searchControl">
                    <p>Search free photos</p>
                    <Input icon placeholder="Search photo">
                      <input />
                      <Icon name="search" />
                    </Input>
                  </div>
                )}
                <p>{menuData.description}</p>
                {menuData.value === "color" && (
                  <div id="colorPalette">
                    {this.state.isColorPickerOpened && (
                      <ChromePicker
                        color={this.state.pickerColor}
                        onChangeComplete={this.handlePickerChange}
                      />
                    )}
                    <div
                      onClick={this.openColorPicker}
                      className="colorPickerButton colorItem"
                    >
                      <div>
                        <img
                          src="/basicImages/colorPicker.png"
                          alt="Color Picker"
                        />
                      </div>
                    </div>
                    {this.state.colorOptions.map((colorCode, index3) => (
                      <div
                        key={index3}
                        className="colorItem"
                        style={{ backgroundColor: "#" + colorCode }}
                        onClick={this.onSelectColorOption(colorCode)}
                      />
                    ))}
                  </div>
                )}
                {menuData.value !== "color" &&
                  (menuData.value !== "myImages" ? (
                    <div className="menuOptions">
                      {menuData.data.map((menuOptions, index2) => {
                        return (
                          <div
                            key={index2}
                            className={
                              menuOptions.value ===
                              this.state.selectedOptions[menuData.value]
                                ? "active"
                                : ""
                            }
                          >
                            <img
                              onClick={() =>
                                this.handleItemBackgroundItemClick(
                                  menuData.value,
                                  menuOptions.value
                                )
                              }
                              key={index2}
                              className="sectionColumnsItem"
                              src={menuOptions.imageURL}
                              alt={menuOptions.value}
                            />
                            <div className="activeItemCover">
                              <div>
                                <img
                                  src="/basicImages/menuItemClick.png"
                                  alt=""
                                />
                              </div>
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  ) : (
                    <div id="imageUpload">
                      <button onClick={this.startUploadImage}>
                        Upload image
                      </button>
                      <input type="file" />
                    </div>
                  ))}
              </div>
            </div>
          </div>
        ))}
      </div>
    );
  }
}
