import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import { createStore, applyMiddleware, compose } from "redux";
import thunkMiddleWare from "redux-thunk";
import { createLogger } from "redux-logger";
import { Provider } from "react-redux";
import designStudioReducer from "./redux/reducers";
import { BrowserRouter, Route } from "react-router-dom";
import App from "./AppContainer";
import registerServiceWorker from "./registerServiceWorker";

let loggerMiddleware = createLogger();

const composeEnhancers =
  typeof window === "object" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        // Specify extension’s options like name, actionsBlacklist, actionsCreators,
        // serialize...
      })
    : compose;

const enhancer = composeEnhancers(
  applyMiddleware(thunkMiddleWare)
  // other store enhancers if any
);

let store = createStore(
  designStudioReducer,
  enhancer,
  applyMiddleware(thunkMiddleWare, loggerMiddleware)
);

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Route render={() => <App />} />
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
