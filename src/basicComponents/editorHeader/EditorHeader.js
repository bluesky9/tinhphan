import React from "react";
import { navigationKeywords } from "../../assets/navigationKeywords";
import { Dropdown } from "semantic-ui-react";
import "../../styles/editorHeader.css";

const dropdownOptions = [
  {
    key: 1,
    text: "Save"
  },
  {
    value: "save",
    key: 2,
    text: "Save as..."
  },
  {
    key: 3,
    text: "Change dataset",
    value: "changeDataset"
  }
];

export default class EditorHeader extends React.Component {
  constructor(props) {
    super();
    this.state = {
      isDropdownOpened: false
    };
  }

  handleDropdownOpen = isOpened => {
    this.setState({ isDropdownOpened: isOpened });
  };

  render() {
    const currentPage = this.props.navigation.currentPage;
    return (
      <div
        className={
          "editorHeader " +
          (currentPage === navigationKeywords.EDITOR ? "" : "inActive")
        }
      >
        <p id="pageLogo">MenuCreator</p>
        <div id="headerButtons">
          <Dropdown
            onOpen={() => this.handleDropdownOpen(true)}
            onClose={() => this.handleDropdownOpen(false)}
            id="fileOptions"
            text="File"
            className={this.state.isDropdownOpened ? "active" : ""}
            options={dropdownOptions}
          />
          <div id="undoButton">
            <img src="/basicImages/undoButton.png" alt="Undo" />
            <p>Undo</p>
          </div>
          <div id="redoButton">
            <img src="/basicImages/redoButton.png" alt="Redo" /> <p>Redo</p>
          </div>
          <p id="helpButton">Help</p>
        </div>
        <p id="menuTitle">Italian Restaurant Delicious Food</p>
        <p id="printButton">Print</p>
        <p id="downloadButton">Download</p>
      </div>
    );
  }
}
