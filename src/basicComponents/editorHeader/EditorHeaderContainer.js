import { connect } from "react-redux";
import EditorHeader from "./EditorHeader";
// import Redux Actions with the following line
// import {} from  ''

const mapStateToProps = (state, ownProps) => {
  return Object.assign(
    {},
    {
      navigation: state.navigation
    }
  );
};

const mapDispatchToProps = dispatch => {
  return {
    // Dispatch here
  };
};

const EditorHeaderContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(EditorHeader);

export default EditorHeaderContainer;
