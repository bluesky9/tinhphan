import React from "react";
import { navigationKeywords } from "../../assets/navigationKeywords";
import "../../styles/pageHeader.css";

export default class PageHeader extends React.Component {
  constructor(props) {
    super();
    this.state = {};
  }

  render() {
    const currentPage = this.props.navigation.currentPage;
    return (
      <div
        className={
          "pageHeader " +
          (currentPage === navigationKeywords.ENRTER_PAGE ||
          currentPage === navigationKeywords.MENU_TEMPLATE
            ? ""
            : "inActive")
        }
      >
        <div
          onClick={() => this.props.backClicked(navigationKeywords.ENRTER_PAGE)}
          id="backButton"
        >
          <img alt="backArrow" src="/basicImages/backArrow.png" />
          <p>Back</p>
        </div>
        <p className="pageTitle">MenuCreator</p>
        <div id="helpButton">
          <img alt="helpButton" src="/basicImages/helpButton.png" />
          <p>Help</p>
        </div>
      </div>
    );
  }
}
