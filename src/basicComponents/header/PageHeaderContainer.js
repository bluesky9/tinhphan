import { connect } from "react-redux";
import {navigatePage} from "../../redux/actions"
import Header from "./PageHeader";
// import Redux Actions with the following line
// import {} from  ''

const mapStateToProps = (state, ownProps) => {
  return Object.assign(
    {},
    {
      navigation: state.navigation
    }
  );
};

const mapDispatchToProps = dispatch => {
  return {
    backClicked: pageNAME => dispatch(navigatePage(pageNAME))
  };
};

const HeaderContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Header);

export default HeaderContainer;
