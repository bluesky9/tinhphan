export const CHECK_HOST = "CHECK_HOST";

export function checkHost(hostname) {
  return {
    type: CHECK_HOST,
    host: hostname
  };
}
