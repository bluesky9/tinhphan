import { CHECK_HOST, checkHost } from "./checkHost";
import * as toolbarActions from "./toolbar";

import {
  NAVIGATE_TO_PAGE,
  NAVIGATE_EDITOR_SECTION,
  NAVIGATE_EDITOR_SUB_SECTION,
  navigateEditorSection,
  navigatePage,
  navigateEditorSubSection
} from "./navigation";

import {
  SELECT_TEMPLATE,
  CREATE_MENU,
  UPDATE_MENU_LAYOUT,
  ZOOM_EDITOR,
  ADD_ELEMENT_ACCENT,
  MOVE_ELEMENT,
  RESIZE_ELEMENT,
  ADD_NEW_MENU_ITEM,
  ADD_NEW_MENU_SECTION,
  CHANGE_EDITOR_PAGE,
  UPDATE_NUMBER_OF_PAGE,
  UPDATE_ZOOM_LEVEL,
  EDIT_SECTION_TITLE,
  EDIT_MENU_ITEM_TEXT,
  ADD_ELEMENT,
  SELECT_ITEM,
  STYLE_ELEMENT,
  FORMAT_TEXT_SECTION,
  templateItemClicked,
  menuCreationHandler,
  updateMenuLayout,
  zoomEditor,
  addElementAccent,
  moveElement,
  resizeElement,
  addNewMenuItem,
  addNewMenuSection,
  changeEditorPage,
  updateNumberOfPage,
  updateZoomLevel,
  editSecitonTitle,
  editMenuItemText,
  addElement,
  selectItem,
  styleElement,
  formatTextSection
} from "./editor";

export {
  CHECK_HOST,
  NAVIGATE_TO_PAGE,
  SELECT_TEMPLATE,
  NAVIGATE_EDITOR_SECTION,
  CREATE_MENU,
  UPDATE_MENU_LAYOUT,
  ZOOM_EDITOR,
  NAVIGATE_EDITOR_SUB_SECTION,
  ADD_ELEMENT_ACCENT,
  MOVE_ELEMENT,
  RESIZE_ELEMENT,
  ADD_NEW_MENU_ITEM,
  ADD_NEW_MENU_SECTION,
  CHANGE_EDITOR_PAGE,
  UPDATE_NUMBER_OF_PAGE,
  UPDATE_ZOOM_LEVEL,
  EDIT_SECTION_TITLE,
  EDIT_MENU_ITEM_TEXT,
  ADD_ELEMENT,
  SELECT_ITEM,
  STYLE_ELEMENT,
  FORMAT_TEXT_SECTION
};

export {
  checkHost,
  navigatePage,
  templateItemClicked,
  navigateEditorSection,
  menuCreationHandler,
  updateMenuLayout,
  zoomEditor,
  navigateEditorSubSection,
  addElementAccent,
  moveElement,
  resizeElement,
  addNewMenuItem,
  addNewMenuSection,
  changeEditorPage,
  updateNumberOfPage,
  updateZoomLevel,
  editSecitonTitle,
  editMenuItemText,
  addElement,
  selectItem,
  styleElement,
  formatTextSection
};

// export actions here
export {
  toolbarActions,
}