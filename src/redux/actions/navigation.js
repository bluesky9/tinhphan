export const NAVIGATE_TO_PAGE = "NAVIGATE_TO_PAGE";

export const NAVIGATE_EDITOR_SECTION = "NAVIGATE_EDITOR_SECTION";

export const NAVIGATE_EDITOR_SUB_SECTION = "NAVIGATE_EDITOR_SUB_SECTION";

export function navigatePage(page) {
  return {
    type: NAVIGATE_TO_PAGE,
    toPage: page
  };
}

export function navigateEditorSection(editorSection) {
  return {
    type: NAVIGATE_EDITOR_SECTION,
    toSection: editorSection
  };
}

export function navigateEditorSubSection(subSection) {
  return {
    type: NAVIGATE_EDITOR_SUB_SECTION,
    subSection
  };
}
