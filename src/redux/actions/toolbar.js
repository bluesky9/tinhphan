const getActionsType = name => `toolbar.actions.${name}`;

export const SET_MENU_BACKGROUND = getActionsType("SET_MENU_BACKGROUND");
export const setMenuBackground = (color) => {
    return {
        type: SET_MENU_BACKGROUND,
        payload: {
            color,
        }
    }
}

export const SET_BACKGROUND_OPTIONS = getActionsType("SET_BACKGROUND_OPTIONS");
export const setBackGroundOptions = (options) => {
    return {
        type: SET_BACKGROUND_OPTIONS,
        payload: {
            options,
        }
    }
}


export const CLEAR_BACKGROUND_OPTIONS = getActionsType("CLEAR_BACKGROUND_OPTIONS");
export const clearBackgroundOptions = () => {
    return {
        type: CLEAR_BACKGROUND_OPTIONS,
    }
}