import { navigatePage } from ".";
import { navigationKeywords } from "../../assets/navigationKeywords";
export const SELECT_TEMPLATE = "SELECT_TEMPLATE";
export const CREATE_MENU = "CREATE_MENU";
export const UPDATE_MENU_LAYOUT = "UPDATE_MENU_LAYOUT";
export const ZOOM_EDITOR = "ZOOM_EDITOR";

function createMenu(menuInfo) {
  return {
    type: CREATE_MENU,
    menuInfo
  };
}

function selectTemplate(templateInfo) {
  return {
    type: SELECT_TEMPLATE,
    templateInfo
  };
}

export function menuCreationHandler(menuInfo) {
  return dispatch => {
    dispatch(createMenu(menuInfo));
    dispatch(navigatePage(navigationKeywords.MENU_TEMPLATE));
  };
}

export function templateItemClicked(templateInfo) {
  return dispatch => {
    dispatch(selectTemplate(templateInfo));
    dispatch(navigatePage(navigationKeywords.EDITOR));
  };
}

export function updateMenuLayout(layoutData) {
  return {
    type: UPDATE_MENU_LAYOUT,
    layoutData: layoutData
  };
}

export function zoomEditor(isIncrement) {
  return {
    type: ZOOM_EDITOR,
    zoomType: isIncrement
  };
}

export const UPDATE_ZOOM_LEVEL = "UPDATE_ZOOM_LEVEL";

export function updateZoomLevel(zoomLevel) {
  return {
    type: UPDATE_ZOOM_LEVEL,
    zoomLevel
  };
}

export const CHANGE_EDITOR_PAGE = "CHAGE_EDITOR_PAGE";

export function changeEditorPage(isIncrement) {
  return {
    type: CHANGE_EDITOR_PAGE,
    isIncrement
  };
}

export const UPDATE_NUMBER_OF_PAGE = "UDPATE_NUMBER_OF_PAGE";

export function updateNumberOfPage(numberOfPage) {
  return {
    type: UPDATE_NUMBER_OF_PAGE,
    numberOfPage
  };
}

export const ADD_ELEMENT_ACCENT = "ADD_ELEMENTS_ACCENTS";

export function addElementAccent(accentData) {
  return {
    type: ADD_ELEMENT_ACCENT,
    accentData
  };
}

export const ADD_ELEMENT = "ADD_ELEMENT";

export function addElement(elementType, elementData, pageNumber) {
  return {
    type: ADD_ELEMENT,
    elementType,
    elementData,
    pageNumber
  };
}

export const MOVE_ELEMENT = "MOVE_ELEMENT";

export function moveElement(elementType, index, changeX, changeY, pageNumber) {
  console.log("pagepage", pageNumber);
  return {
    type: MOVE_ELEMENT,
    changeX,
    changeY,
    elementType: elementType,
    index,
    pageNumber: pageNumber - 1
  };
}

export const RESIZE_ELEMENT = "RESIZE_ELEMENT";

export function resizeElement(elementType, index, changes, pageNumber) {
  return {
    type: RESIZE_ELEMENT,
    changes,
    index,
    elementType,
    pageNumber: pageNumber - 1
  };
}

// Handle Text (Menu Section, Menu Item)

export const ADD_NEW_MENU_ITEM = "ADD_NEW_MENU_ITEM";

export function addNewMenuItem(sectionPos) {
  return {
    type: ADD_NEW_MENU_ITEM,
    sectionPos
  };
}

export const ADD_NEW_MENU_SECTION = "ADD_NEW_MENU_SECTION";

export function addNewMenuSection(sectionData) {
  return {
    type: ADD_NEW_MENU_SECTION,
    sectionData
  };
}

export const EDIT_SECTION_TITLE = "EDIT_SECTION_TITLE";

export function editSecitonTitle(sectionId, value) {
  return {
    type: EDIT_SECTION_TITLE,
    sectionId,
    value
  };
}

export const EDIT_MENU_ITEM_TEXT = "EDIT_MENU_ITEM_TEXT";

export function editMenuItemText(itemId, textType, value) {
  return {
    type: EDIT_MENU_ITEM_TEXT,
    itemId,
    textType,
    value
  };
}

export const SELECT_ITEM = "SELECT_ITEM";

export function selectItem(itemData) {
  return {
    type: SELECT_ITEM,
    itemData
  };
}

export const STYLE_ELEMENT = "STYLE_ELEMENT";

export function styleElement(index, styleData, pageNumber) {
  return {
    type: STYLE_ELEMENT,
    styleData,
    index,
    pageNumber
  };
}

export const FORMAT_TEXT_SECTION = "FORMAT_TEXT_SECTION";

export function formatTextSection(sectionIndex, formatData) {
  return {
    type: FORMAT_TEXT_SECTION,
    sectionIndex,
    formatData
  };
}
