import {
    CHECK_HOST
} from "../actions"

export function hostname(state = {
    host: "localhost:3000"
}, action) {
    switch (action.type) {
        case CHECK_HOST:
            return Object.assign({}, state, {
                host: action.host
            });
        default:
            return state;
    }
}