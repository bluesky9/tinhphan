import {
  NAVIGATE_TO_PAGE,
  NAVIGATE_EDITOR_SECTION,
  NAVIGATE_EDITOR_SUB_SECTION
} from "../actions";

import {
  navigationKeywords,
  editorSections,
  editorSubSections
} from "../../assets/navigationKeywords";

export function navigation(
  state = {
    currentPage: navigationKeywords.EDITOR,
    editorSection: editorSections.TEXT,
    editorSubsection: ""
  },
  action
) {
  switch (action.type) {
    case NAVIGATE_TO_PAGE:
      return Object.assign({}, state, { currentPage: action.toPage });
    case NAVIGATE_EDITOR_SECTION:
      return Object.assign({}, state, {
        editorSection: action.toSection,
        editorSubsection: ""
      });
    case NAVIGATE_EDITOR_SUB_SECTION:
      return Object.assign({}, state, {
        editorSubsection: action.subSection
      });
    default:
      return state;
  }
}
