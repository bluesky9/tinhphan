import {
  SELECT_TEMPLATE,
  CREATE_MENU,
  UPDATE_MENU_LAYOUT,
  ZOOM_EDITOR,
  ADD_ELEMENT_ACCENT,
  MOVE_ELEMENT,
  RESIZE_ELEMENT,
  ADD_NEW_MENU_SECTION,
  ADD_NEW_MENU_ITEM,
  CHANGE_EDITOR_PAGE,
  UPDATE_NUMBER_OF_PAGE,
  UPDATE_ZOOM_LEVEL,
  EDIT_SECTION_TITLE,
  EDIT_MENU_ITEM_TEXT,
  ADD_ELEMENT,
  SELECT_ITEM,
  STYLE_ELEMENT,
  FORMAT_TEXT_SECTION,
  // Actions in here
  toolbarActions
} from "../actions";
import { layoutData } from "../../components/editor/sectionToolbar/layoutToolbar/layoutData";

const defaultState = {
  templateInfo: {},
  menuInfo: {},
  menuData: {
    layout: {
      style: layoutData[0].value,
      value: layoutData[0].data[0].value,
      sizeValue: layoutData[0].data[0].sizeValue
    },
    pageData: [
      {
        elements: {
          accents: [],
          illustrations: [],
          stockPhotos: [],
          shapes: []
        }
      }
    ],
    text: [],
    textUpdate: 0,
    numberOfPage: 1,
    background: {
      color: "",
      stockPhotos: "",
      texttures: "",
    }
  },
  zoomLevel: 1,
  page: 1,
  selectedItem: {}
};

export function editor(
  state = defaultState,
  action
) {
  const menuData = Object.assign({}, state.menuData);
  const elementStyle = {
    height: "45px",
    width: "45px",
    left: "12px",
    top: "12px",
    opacity: 1
  };

  const { payload } = action;

  switch (action.type) {
    case toolbarActions.CLEAR_BACKGROUND_OPTIONS: {
      return {
        ...state,
        menuData: {
          ...state.menuData,
          background: defaultState.menuData.background
        }
      }
    }

    case toolbarActions.SET_MENU_BACKGROUND: {
      return {
        ...state,
        menuData: {
          ...state.menuData,
          background: {
            ...state.menuData.background,
            color: payload.color
          }
        }
      }
    }

    case toolbarActions.SET_BACKGROUND_OPTIONS: {
      return {
        ...state,
        menuData: {
          ...state.menuData,
          background: {
            ...state.menuData.background,
            [payload.options.optionName]: payload.options.data
          }
        }
      }
    }

    case SELECT_TEMPLATE:
      return Object.assign({}, state, {
        templateInfo: action.templateInfo
      });
    case CREATE_MENU:
      {
        return Object.assign({}, state, {
          menuInfo: action.menuInfo,
          menuData: {
            ...state.menuData,
            layout: {
              style: action.menuInfo.menuFormat,
              value: action.menuInfo.menuFormatItem,
              sizeValue: layoutData.find(item => item.value == action.menuInfo.menuFormat).data.find(item => item.value == action.menuInfo.menuFormatItem).sizeValue,
            }
          }
        });
      }
    case UPDATE_MENU_LAYOUT:
      menuData.layout = Object.assign({}, menuData.layout, {
        style: action.layoutData.style,
        value: action.layoutData.value,
        sizeValue: action.layoutData.sizeValue
      });
      return Object.assign({}, state, {
        menuData
      });
    case ZOOM_EDITOR:
      return Object.assign({}, state, {
        zoomLevel: action.zoomType
          ? state.zoomLevel + 0.1
          : state.zoomLevel - 0.1
      });
    case UPDATE_ZOOM_LEVEL:
      return Object.assign({}, state, {
        zoomLevel: action.zoomLevel
      });
    case ADD_ELEMENT_ACCENT:
      const newAccentData = Object.assign({}, action.accentData, {
        style: elementStyle
      });
      menuData.elements.accents.push(newAccentData);
      return Object.assign({}, state, { menuData });
    case ADD_ELEMENT:
      const newElement = Object.assign({}, action.elementData, {
        style: elementStyle
      });
      menuData.pageData[action.pageNumber].elements[action.elementType].push(
        newElement
      );
      return Object.assign({}, state, {
        menuData
      });
    case MOVE_ELEMENT:
      const newElementSet = menuData.pageData[action.pageNumber].elements[
        action.elementType
      ].map((element, index) => {
        if (action.index === index) {
          const style = Object.assign({}, element.style);
          style.top = Number(style.top.split("px")[0]) + action.changeY + "px";
          style.left =
            Number(style.left.split("px")[0]) + action.changeX + "px";
          console.log(style);

          const newElementData = Object.assign({}, element, { style });
          return newElementData;
        } else {
          return element;
        }
      });
      menuData.pageData[action.pageNumber].elements[
        action.elementType
      ] = newElementSet;
      return Object.assign({}, state, { menuData });
    case RESIZE_ELEMENT:
      const newElememtSet = menuData.pageData[action.pageNumber].elements[
        action.elementType
      ].map((element, index) => {
        if (action.index === index) {
          const style = Object.assign({}, element.style);
          style.top =
            Number(style.top.split("px")[0]) + action.changes[3] + "px";
          style.left =
            Number(style.left.split("px")[0]) + action.changes[2] + "px";
          style.height =
            Number(style.height.split("px")[0]) + action.changes[1] + "px";
          style.width =
            Number(style.width.split("px")[0]) + action.changes[0] + "px";

          const newElementData = Object.assign({}, element, { style });
          return newElementData;
        } else {
          return element;
        }
      });
      menuData.pageData[action.pageNumber].elements[
        action.elementType
      ] = newElememtSet;
      return Object.assign({}, state, { menuData });
    /*      if (action.elementType === "accent") {
        const newAccents = menuData.elements.accents.map((accent, index) => {
          if (action.index === index) {
            const style = Object.assign({}, accent.style);
            style.top =
              Number(style.top.split("px")[0]) + action.changes[3] + "px";
            style.left =
              Number(style.left.split("px")[0]) + action.changes[2] + "px";
            style.height =
              Number(style.height.split("px")[0]) + action.changes[1] + "px";
            style.width =
              Number(style.width.split("px")[0]) + action.changes[0] + "px";

            const newAccentData = Object.assign({}, accent, { style });
            return newAccentData;
          } else {
            return accent;
          }
        });
        menuData.elements.accents = newAccents;
      }
      return Object.assign({}, state, { menuData });*/
    case ADD_NEW_MENU_SECTION:
      const sections = [...state.menuData.text];
      sections.push(
        Object.assign({}, action.sectionData, {
          data: [],
          formatData: {}
        })
      );
      menuData.text = sections;
      menuData.textUpdate++;
      return Object.assign({}, state, { menuData });
    case ADD_NEW_MENU_ITEM:
      const sections2 = state.menuData.text.map((sectionData, index) => {
        if (index === action.sectionPos) {
          const newItem = {
            name: "New Dish",
            description: "Description",
            price: 10
          };
          sectionData.data.push(newItem);
          return sectionData;
        } else {
          return sectionData;
        }
      });
      menuData.text = sections2;
      menuData.textUpdate++;
      return Object.assign({}, state, { menuData });
    case CHANGE_EDITOR_PAGE:
      return Object.assign(
        {},
        state,
        editorPagination(state, action.isIncrement)
      );
    case UPDATE_NUMBER_OF_PAGE:
      return Object.assign({}, state, {
        menuData: updateNumberOfPage(menuData, action.numberOfPage)
      });
    case EDIT_SECTION_TITLE:
      return Object.assign({}, state, {
        menuData: editSectionTitle(menuData, action.sectionId, action.value)
      });
    case EDIT_MENU_ITEM_TEXT:
      return Object.assign({}, state, {
        menuData: editMenuItemText(
          menuData,
          action.itemId,
          action.textType,
          action.value
        )
      });
    case SELECT_ITEM:
      return Object.assign({}, state, selectItem(state, action.itemData));
    case STYLE_ELEMENT:
      return Object.assign({}, state, {
        menuData: styleElement(
          menuData,
          action.index,
          action.styleData,
          action.pageNumber
        )
      });
    case FORMAT_TEXT_SECTION:
      return Object.assign({}, state, {
        menuData: formatTextSection(
          menuData,
          action.sectionIndex,
          action.formatData
        )
      });
    default:
      return state;
  }
}

function editorPagination(editorState, isIncrement) {
  const newEditorState = Object.assign({}, editorState);
  if (isIncrement) {
    if (editorState.page < editorState.menuData.numberOfPage) {
      newEditorState.page++;
    }
  } else {
    if (newEditorState.page !== 1) {
      newEditorState.page--;
    }
  }

  return newEditorState;
}

function updateNumberOfPage(menuData, numberOfPage) {
  const pageData = menuData.pageData;
  if (numberOfPage > menuData.numberOfPage) {
    pageData.push({
      elements: {
        accents: [],
        illustrations: [],
        stockPhotos: [],
        shapes: []
      }
    });
  } else if (numberOfPage < menuData.numberOfPage) {
    pageData.splice(pageData.length - 1, 1);
  }
  return Object.assign({}, menuData, {
    numberOfPage,
    pageData
  });
}

function editSectionTitle(menuData, sectionId, value) {
  const newMenuData = Object.assign({}, menuData);

  newMenuData.text = menuData.text.map((section, index) => {
    if (index === sectionId) {
      section.name = value;
      return section;
    } else {
      return section;
    }
  });
  return newMenuData;
}

function editMenuItemText(menuData, rawItemId, type, value) {
  const newMenuData = Object.assign({}, menuData);

  const itemCords = rawItemId.split(".");
  const sectionId = Number(itemCords[0]);
  const itemId = Number(itemCords[1]);

  newMenuData.text = newMenuData.text.map((section, sectionIndex) => {
    if (sectionId === sectionIndex) {
      section.data = section.data.map((item, itemIndex) => {
        if (itemId === itemIndex) {
          item[type] = value;
          return item;
        } else {
          return item;
        }
      });
      return section;
    } else {
      return section;
    }
  });

  return newMenuData;
}

export function selectItem(state, itemData) {
  const newState = Object.assign({}, state);
  newState.selectedItem = Object.assign({}, itemData);
  return newState;
}

function styleElement(menuData, index, styleData, pageNumber) {
  const newElementSet = menuData.pageData[pageNumber].elements[
    styleData.type
  ].map((element, elementIndex) => {
    if (elementIndex === index) {
      const style = Object.assign({}, element.style);
      style[styleData.styleType] = styleData.value;
      const newElementData = Object.assign({}, element, { style });
      return newElementData;
    } else {
      return element;
    }
  });

  menuData.pageData[pageNumber].elements[styleData.type] = newElementSet;

  return menuData;
}

function formatTextSection(menuData, sectionIndex, formatData) {
  const newMenuData = Object.assign({}, menuData);

  newMenuData.text.map((sectionData, index) => {
    if (index === sectionIndex) {
      const newSectionData = Object.assign({}, sectionData);
      newSectionData.formatData[formatData.type] = formatData.value;
      return newSectionData;
    } else {
      return sectionData;
    }
  });

  return newMenuData;
}

function extractItemId(itemId) {
  const itemCords = itemId.split(".");
  itemCords[0] = Number(itemCords[0]);
  itemCords[1] = Number(itemCords[1]);

  return itemCords;
}
