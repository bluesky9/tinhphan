import { hostname } from "./checkHost";
import { navigation } from "./navigation";
import { editor } from "./editor";
import { combineReducers } from "redux";

const designStudio = combineReducers({
  hostname,
  navigation,
  editor
});

export default designStudio;
