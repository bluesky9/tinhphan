import { connect } from "react-redux";
import App from "./App";
import { withRouter } from "react-router-dom";
import { checkHost } from "./redux/actions";

const mapStateToProps = (state, ownProps) => {
  return {
    navigation: state.navigation
  };
};

const mapDispatchToProps = dispatch => {
  return {
    checkHost: host => dispatch(checkHost(host))
  };
};

const AppContainer = withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(App)
);

export default AppContainer;
