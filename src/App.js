import React, { Component } from "react";
// import logo from "./logo.svg";
import "./App.css";
import Header from "./basicComponents/header/PageHeaderContainer";
import EditorHeader from "./basicComponents/editorHeader/EditorHeaderContainer";
import EnterPage from "./components/enterPage/EnterPageContainer";
import MenuTemplate from "./components/menuTemplatePage/MenuTemplatePageContainer";
import EditorPage from "./components/editor/EditorPageContainer";

class App extends Component {
  componentDidMount() {
    const hostname = window.location.hostname;
    if (hostname === "localhost") {
      this.props.checkHost("localhost");
    } else {
      this.props.checkHost("production");
    }
  }

  render() {
    return (
      <div className="App">
        <Header />
        <EditorHeader />
        <div id="mainAppArea">
          <EnterPage />
          <MenuTemplate />
          <EditorPage />
        </div>
      </div>
    );
  }
}

export default App;
