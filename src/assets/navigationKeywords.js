export const navigationKeywords = {
  ENRTER_PAGE: "enter",
  MENU_TEMPLATE: "template",
  EDITOR: "editor"
};

export const editorSections = {
  LAYOUT: "layout",
  SETUP: "setup",
  FORMAT: "format",
  TEXT: "text",
  ELEMENT: "element",
  BACKGROUND: "background"
};

export const editorSubSections = {
  TEXT: "TEXT",
  ELEMENTS_ACCENT: "ELEMENTS_ACCENT",
  ELEMENTS_ILLUSTRATION: "ELEMENTS_ILLUSTRATION",
  ELEMENTS_SHAPE: "ELEMENTS_SHAPE"
};
