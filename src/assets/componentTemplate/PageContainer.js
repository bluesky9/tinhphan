import { connect } from "react-redux";
import Page from "./Page";
// import Redux Actions with the following line
// import {} from  ''

const mapStateToProps = (state, ownProps) => {
  return Object.assign(
    {},
    {
      // List props here
    }
  );
};

const mapDispatchToProps = dispatch => {
  return {
    // Dispatch here
  };
};

const PageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Page);

export default PageContainer;
